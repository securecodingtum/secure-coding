<?php

require_once('includes/db.php');
require_once("includes/class.employee.php");
require_once("includes/class.user.php");
require_once("includes/class.account.php");
require_once("includes/class.transaction.php");
require_once('includes/sanitize.php');
require_once("includes/class.csrf.php");

session_start();

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isEmployee') {
    $_SESSION['from'] = "employee.php";
    header("Location: empLogin.php");
    exit;
}
$user = new Employee();
$user->load_info($_SESSION['username']);

$success = null;
$trans = new Transactions();
$acc = new Accounts();
$csrf = new Csrf();
if (isset($_GET['type']) && isset($_GET['id'])) {
	if(!$csrf->verifyToken()){
		$success = 'Error processing request.';
	}else{
		$_GET = sanitize_html($_GET);
		$editedTrans = $trans->get_by_id($_GET['id']);
		if ($editedTrans !== null) {
			if ($_GET['type'] === 'accept') {
				$account = $acc->get_by_id($editedTrans->fromAcc);
				if($account->amount - $editedTrans->amount <0){
					$success = "Insufficient Balance. Cannot accept this transaction.";
				}elseif($editedTrans->status != 'pending'){
					$success = "Cannot re-approved already approved transaction.";
				}else{
					$editedTrans->status = 'accepted';
					$account->amount -= $editedTrans->amount;
					$editedTrans->store();
					$account->store();
					$account = $acc->get_by_numeration($editedTrans->toAcc);
					if (sizeof($account) > 0) {
						$account[0]->amount += $editedTrans->amount;
						$account[0]->store();
					}
					$success = 'Transaction accepted successfully.';
				}
			} else if ($_GET['type'] === 'reject') {
				if($editedTrans->status != 'pending'){
						$success = "Cannot reject already approved transaction.";
				}else{
					$editedTrans->delete();
					$success = 'Transaction rejected successfully.';
				}
			}
		}
	}
}

$transactions = $trans->get_pending();

$clients = array();
$users = new Users();
foreach ($transactions as $transaction) {
    if (!isset($clients[$transaction->client])) {
        $client = $users->get_by_id($transaction->client);
        $clients[$transaction->client] = $client->surname . ", " . $client->name;
    }
}

$fromAccounts = array();
foreach ($transactions as $transaction) {
    if (!isset($accounts[$transaction->client])) {
        $account = $acc->get_by_id($transaction->fromAcc);
        $fromAccounts[$transaction->fromAcc] = $account->numeration;
    }
}

$csrf->setToken();
session_regenerate_id();
require('views/empTransactions.php');
?>

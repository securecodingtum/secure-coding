<?php

class Employee
{
    public $username;
    private $hash;

    public $name;
    public $surname;
	
	private $invalidAttempts;
	private $timestamp;

    function set_password($password)
    {
        $this->hash = password_hash($password, PASSWORD_DEFAULT);
    }

    function verify_password($password)
    {
        return password_verify($password, $this->hash);
    }

    function login($password)
    {
        if (password_verify($password, $this->hash)) {
            return true;
        } else {
            return false;
        }
    }

    function approve()
    {
        $this->status = 'approved';
        $this->store();
    }

    function init($username, $password, $firstName, $surname)
    {
        $this->username  = $username;
        $this->firstName = $firstName;
        $this->surname   = $surname;
        $this->set_password($password);
    }

    function name_available($username)
    {
        global $db;

        $st = $db->prepare('SELECT count(*) as rows FROM employee WHERE username = ?');
        $st->execute(array($username));
        $res = $st->fetchAll()[0];
        $rows = $res["rows"];

        if ($rows == 0) {
            return true;
        }
        return false;
    }

    function load_all()
    {
        global $db;
        $st = $db->prepare('SELECT * FROM employee ORDER BY surname ASC, firstName ASC');
        $st->execute();
        $res = $st->fetchAll();
        $result = [];
        foreach ($res as $row) {
            $employee = new Employee();
            $employee->username  = $row['username'];
            $employee->firstName = $row['firstName'];
            $employee->surname   = $row['surname'];
            $result []= $employee;
        }
        return $result;
    }

    function load_info($username)
    {
        global $db;
        $st = $db->prepare('SELECT * FROM employee WHERE username = ?');
        $st->execute(array($username));

        $res = $st->fetchAll();
        if(sizeof($res) > 0) {
            $res = $res[0];
            $this->username  = $res["username"];
            $this->hash      = $res["password"];
            $this->firstName = $res["firstName"];
            $this->surname   = $res["surname"];
			$this->timestamp = $res["timestamp"];
			$this->invalidAttempts = $res["invalidAttempts"];
        }
    }

    function store()
    {
        global $db;
        if ($this->name_available($this->username)) {
            $st = $db->prepare('INSERT INTO employee (username, password, firstName, surname) VALUES (?, ?, ?, ?)');
            $st->execute(array($this->username, $this->hash, $this->firstName, $this->surname));
        } else {
            $st = $db->prepare('UPDATE employee SET password = ?, firstName = ?, surname = ? WHERE username = ?');
            $st->execute(array($this->hash, $this->firstName, $this->surname, $this->username));
        }
    }
    
    function remove() {
        global $db;
        if (!$this->name_available($this->username)) {
            $st = $db->prepare('DELETE FROM employee WHERE username = ?');
            $st->execute(array($this->username));
        }
    }
	
	function updateLoginAttempts($invalidAttempts,$timestamp){
		global $db;
		$st = $db->prepare('UPDATE employee SET invalidAttempts = ?, timestamp = ? where username = ?');
		$st->execute(array($invalidAttempts,$timestamp,$this->username));
	}
	
	function getInvalidAttempts(){
		return $this->invalidAttempts;
	}
	
	function getTimestamp(){
		return $this->timestamp;
	}
}

?>

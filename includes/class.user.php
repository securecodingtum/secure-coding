<?php

class Users {

    function get_by_id($id) {
        global $db;
        $st = $db->prepare('SELECT * FROM client WHERE id = ?');
        $st->execute(array($id));
        $res = $st->fetchAll();
        if (sizeof($res) === 0) {
            return null;
        }
        $user = new User();
        $user->load_info($res[0]['username']);
        return $user;
    }

    function get_by_username($username) {
        global $db;
        $st = $db->prepare('SELECT username FROM client WHERE username = ?');
        $st->execute(array($username));
        $res = $st->fetchAll();
        if (sizeof($res) === 0) {
            return null;
        }
        $user = new User();
        $user->load_info($res[0]['username']);
        return $user;
    }

    function get_by_status($status) {
        global $db;
        $st = $db->prepare('SELECT username FROM client WHERE status = ?');
        $st->execute(array($status));

        $users = array();

        foreach ($st->fetchAll() as $row) {
            $user = new User();
            $user->load_info($row['username']);
            array_push($users, $user);
        }

        return $users;
    }

    function get_verified() {
        return $this->get_by_status('verified');
    }
}

class User {
    public $id;
    public $email;
    public $username;
    private $hash;
    public $status;
    public $verificationKey; // XXX change back to private

    public $name;
    public $surname;
    public $address;
    public $gender;
    public $passport;

    public $secret;
    public $scspin;
	
	private $invalidAttempts;
	private $timestamp;

    function set_password($password) {
        $this->hash = password_hash($password, PASSWORD_DEFAULT);
    }

    function login($password) {
        if (password_verify($password, $this->hash)) {
            return true;
        } else {
            return false;
        }
    }

    function generate_vkey() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < 32; $i++) {
            $randomString .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        $this->verificationKey = $randomString;
    }

    function generate_secret() {
        $characters = '0123456789';
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        $this->secret = $randomString;
    }

    function generate_scspin() {
        $characters = '0123456789';
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        $this->scspin = $randomString;
    }

    function verify($key) {
        if (!empty($this->verificationKey) && $key == $this->verificationKey) {
            $this->status = 'verified';
            $this->verificationKey = '';
            $this->store();
            return true;
        } else {
            return false;
        }
    }

    function approve() {
        $this->status = 'approved';
        $this->store();
    }

    function init($username, $password, $passport, $email, $name, $surname, $gender, $address) {
        $this->username = $username;
        $this->name = $name;
        $this->passport = $passport;
        $this->surname = $surname;
        $this->gender = $gender;
        $this->address = $address;
        $this->email = $email;
        $this->status = 'unverified';
        $this->set_password($password);
        $this->generate_vkey();
        $this->generate_secret();
        $this->generate_scspin();
        // XXX
    }

    function name_available($username) {
        global $db;

        $st = $db->prepare('SELECT count(*) as rows FROM client WHERE username = ?');
        $st->execute(array($username));
        $res = $st->fetchAll()[0];
        $rows = $res["rows"];

        if ($rows == 0) {
            return true;
        }
        return false;
    }

    function load_info($username) {
        global $db;
        $st = $db->prepare('SELECT * FROM client WHERE username = ?');
        $st->execute(array($username));

        $res = $st->fetchAll();
        if (sizeof($res) > 0) {
            $res = $res[0];

            $this->id = $res['id'];
            $this->username = $res["username"];
            $this->email = $res["email"];
            $this->hash = $res["password"];
            $this->name = $res["firstName"];
            $this->surname = $res["surname"];
            $this->address = $res["address"];
            $this->gender = $res["gender"];
            $this->status = $res["status"];
            $this->passport = $res["passport"];
            $this->verificationKey = $res["verificationKey"];
            $this->secret = $res["secret"];
            $this->scspin = $res["scspin"];
			$this->timestamp = $res["timestamp"];
			$this->invalidAttempts = $res["invalidAttempts"];
        }
    }

    function store() {
        global $db;

        if ($this->name_available($this->username)) {
            $st = $db->prepare('INSERT INTO client (username, password, passport, email, firstName, surname, address, gender, secret, scspin, verificationKey) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
            $st->execute(array($this->username, $this->hash, $this->passport, $this->email, $this->name, $this->surname, $this->address, $this->gender, $this->secret, $this->scspin, $this->verificationKey));
        } else {
            $st = $db->prepare('UPDATE client SET password = ?, passport = ?, email = ?, firstName = ?, surname = ?, address = ?, gender = ?, status = ?, verificationKey = ?, secret = ?, scspin = ? WHERE username = ?');
            $st->execute(array($this->hash, $this->passport, $this->email, $this->name, $this->surname, $this->address, $this->gender, $this->status, $this->verificationKey, $this->secret, $this->scspin, $this->username));

        }
    }
    
    function updatePassword(){
    	global $db;
    	$st = $db->prepare('UPDATE client SET password = ? WHERE username = ?');
    	$st->execute(array($this->hash, $this->username));
    }

    function delete() {
        global $db;

        if (!$this->name_available($this->username)) {
            $st = $db->prepare('DELETE FROM client WHERE username = ?');
            $st->execute(array($this->username));
        }
    }
	
	function updateLoginAttempts($invalidAttempts,$timestamp){
		global $db;
		$st = $db->prepare('UPDATE client SET invalidAttempts = ?, timestamp = ? where username = ?');
		$st->execute(array($invalidAttempts,$timestamp,$this->username));
	}
	
	function getInvalidAttempts(){
		return $this->invalidAttempts;
	}
	
	function getTimestamp(){
		return $this->timestamp;
	}
}

?>
<?php

class TransactionCodes {

    function get_for_client($client) {
        global $db;

        $st = $db->prepare('SELECT * FROM transactionCode WHERE client = ? ORDER BY number ASC');
        $st->execute(array($client));
        $res = $st->fetchAll();
        return $res;
    }

    // XXX: fails when there are no codes left
    function get_random($client) {
        global $db;
        $st = $db->prepare('SELECT number FROM transactionCode WHERE client = ? ORDER BY RAND() LIMIT 1');
        $st->execute(array($client));
        $res = $st->fetchAll();
        if (sizeof($res) === 0) {
            return null;
        }

        return $res[0][0];
    }

    function verify_code($client, $number, $code) {
        global $db;
        $st = $db->prepare('SELECT code FROM transactionCode WHERE number = ? AND client = ?');
        $st->execute(array($number, $client));
        $res = $st->fetchAll();
        if (sizeof($res) === 0) {
            return false;
        }
        $st = $db->prepare('DELETE FROM transactionCode where number = ? AND client = ?');
        $st->execute(array($number, $client));
        return $res[0][0] == $code;
    }

    function generate_tan() {
        /*$characters = '0123456789';
        $randomString = '';
        for ($i = 0; $i < 15; $i++) {
            $randomString .= $characters[mt_rand(0, strlen($characters) - 1)];
        }*/
		$randomString = openssl_random_pseudo_bytes(15);
		$randomString = bin2hex($randomString);
		$randomString = base_convert($randomString ,16,10);
		$randomString = substr($randomString,0,15);
        return $randomString;
    }

    function generate_codes($client, $number) {
        global $db;
        // delete old tans first
        $st = $db->prepare('DELETE FROM transactionCode where client = ?');
        $st->execute(array($client));

        // generate new ones
        $numbers = array();
        for (; $number > 0; $number--) {
            $numbers []= $this->generate_tan();
        }

        // save them
        for ($i = 0; $i < sizeof($numbers); $i++) {
            $st = $db->prepare('INSERT INTO transactionCode (client, number, code) VALUES (?, ?, ?)');
            $st->execute(array($client, $i + 1, $numbers[$i]));
        }
    }

    function verify_scs($scstan, $username, $sender, $receiver, $amount) {
        $secret = $this->get_secret($username);
        $scspin = $this->get_scspin($username);
        $date = new DateTime();
        $time = floor($date->getTimestamp()/(60*5))*60;
        $transaction = $username . ';' . $secret . ';' . $sender . ';' . $receiver . ';' . $amount . ';' . $time . ';' . $scspin;
        $hash = md5($transaction);
        error_log("transaction: '" . $transaction . "'; hash: '" . $hash . "'");
        $tan = strtolower(substr($hash, 0, 15));
        return $scstan == $tan;
    }

    function verify_scs_batch($scstan, $username, $file) {
        $secret = $this->get_secret($username);
        $scspin = $this->get_scspin($username);
        $file_hash = md5_file($file);
        $date = new DateTime();
        $time = floor($date->getTimestamp()/(60*5))*60;
        $transaction = $username . ';' . $secret . ';' . $file_hash . ';' . $time . ';' . $scspin;
        $hash = md5($transaction);
        error_log("transaction: '" . $transaction . "'; hash: '" . $hash . "'");
        $tan = strtolower(substr($hash, 0, 15));
        error_log("tan: " . $tan . "; scstan: " . $scstan);
        return $scstan == $tan;
    }

    function get_secret($username) {
        global $db;
        $st = $db->prepare('SELECT secret FROM client WHERE username = ?');
        $st->execute(array($username));
        $res = $st->fetchAll();

        return $res[0][0];
    }

    function get_scspin($username) {
        global $db;
        $st = $db->prepare('SELECT scspin FROM client WHERE username = ?');
        $st->execute(array($username));
        $res = $st->fetchAll();

        return $res[0][0];
    }
}

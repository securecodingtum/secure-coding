<?php
class Csrf{
	function verifyToken(){
		if (isset($_SESSION['token_value']) && isset($_SESSION['token_time'])){
			/*
			 * if token age is more than 5min destroy it
			 */
			if(time() - $_SESSION['token_time']> 300){
				return false;
			}else {
				/*
				 * first check the type of request
				 */
				$tokenCheck=null;
				if($_SERVER['REQUEST_METHOD'] === 'POST'){
					if(isset($_POST['token'])){
						$tokenCheck = $_POST['token'];
					}else {
						return false;
					}
				}else{
					if(isset($_GET['token'])){
						$tokenCheck = $_GET['token'];
					}else {
						return false;
					}
				}
				/*
				 * check if the value in request is same as stored in session
				 */
				if ($tokenCheck == $_SESSION['token_value']){
					return true;
				}
				else {
					return false;
				}
			}
		}
	}

	function setToken(){
		$_SESSION['token_value'] = password_hash(uniqid(mt_rand(), TRUE),PASSWORD_DEFAULT);
		$_SESSION['token_time'] = time();
	}
}

<?php

class Admin {

    function login($username, $password) {
        global $db;
		global $msg;
        $st = $db->prepare('SELECT * FROM admin');
        $st->execute(array($username));
        $res = $st->fetchAll();
        if (sizeof($res) === 0) {
            return false;
        }
        if (!password_verify($username, $res[0]['username'])) {
            return false;
        }
        if (!password_verify($password, $res[0]['password'])) {
			$timestamp = date('Y-m-d G:i:s');
			$st = $db->prepare('UPDATE admin SET invalidAttempts = ?, timestamp = ? where username = ?');
			$st->execute(array($res[0]['invalidAttempts']+1,$timestamp,$res[0]['username']));
            return false;
        }
		if($res[0]['invalidAttempts']>=3 && time()-strtotime(res[0]['timestamp']) < 3600){
			$msg = "Account is locked. Attempt after sometime";
			return false;
		}elseif($res[0]['invalidAttempts']>0){
			$st = $db->prepare('UPDATE admin SET invalidAttempts = ? where username = ?');
			$st->execute(array(0,$res[0]['username']));
		}
        return true;
    }
}

?>

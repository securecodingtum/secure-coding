<?php
function password_complexity($password)
{
    if (strlen($password) < 8) {
        return false;
    }
    $numbers = 0;
    $lowercase = 0;
    $uppercase = 0;
    $symbols = 0;
    for ($i = 0; $i < strlen($password); $i++) {
        $char = substr($password, $i, 1);
        if (strpos('0123456789', $char) !== false) {
            $numbers++;
        } else if (strpos('abcdefghijklmnopqrstuvwxyz', $char) !== false) {
            $lowercase++;
        } else if (strpos('ABCDEFGHIJKLMNOPQRSTUVWXYZ', $char) !== false) {
            $uppercase++;
        } else {
            $symbols++;
        }
    };

    $variety = 0;
    if ($numbers > 0) $variety++;
    if ($lowercase > 0) $variety++;
    if ($uppercase > 0) $variety++;
    if ($symbols > 0) $variety++;

    if ($variety > 2) {
        return true;
    } else {
        return false;
    }
}
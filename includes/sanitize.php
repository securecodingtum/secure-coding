<?php
function sanitize_html($array) {
    $array = filter_var_array($array, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    // Since I couldn't find a PHP sanitizing filter that filtered out slashes, here's my own attempt at removing these.
    // According to OWASP, slashes are evil and deserve suffering a cruel death rather than being carelessly included
    // in HTML files. See https://www.owasp.org/index.php/XSS_%28Cross_Site_Scripting%29_Prevention_Cheat_Sheet
    foreach ($array as $key => $element) {
        $array[$key] = str_replace('/', "&#x2F;", $element);
    }
    return $array;
}
<?php

class Captcha {

    public function init() {
        require_once('includes/Captcha/Captcha.php');
        require_once('includes/Captcha/Exception.php');
        require_once('includes/Captcha/Response.php');
        $captcha = new Captcha\Captcha();
        $captcha->setPublicKey('6Leqd_4SAAAAAM5t9lbTf-nMYMUdy0xsqb53zeRV');
        $captcha->setPrivateKey('6Leqd_4SAAAAADOTn6QKntB8mgi41ghhQMt4MpOp');
        return $captcha;
    }

} 
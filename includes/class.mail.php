<?php

class Mail {
    function send ($title, $body, $email,$attachment) {
        require_once('includes/class.smtp.php');
        require_once('includes/class.phpmailer.php');

        $mail = new PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
//         $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
        $mail->Host = "mail.gmx.de";
        $mail->Port = 465; // or 587
        $mail->IsHTML(true);
        $mail->Username = "sarah.80803@gmx.de";
        $mail->Password = "Blamuh123";
        $mail->From = "sarah.80803@gmx.de";
        $mail->Subject = $title;
        $mail->Body =  $body;
        if(!is_null($attachment))
        	$mail->AddStringAttachment($attachment,'TAN.pdf');
        $mail->AddAddress($email);
        if(!$mail->Send()){
            return false;
        }
        return true;
    }
}

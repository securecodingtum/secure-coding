SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `securecoding` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `securecoding` ;

-- -----------------------------------------------------
-- Table `securecoding`.`admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `securecoding`.`admin` (
  `id` ENUM('onetrueandonly') NOT NULL DEFAULT 'onetrueandonly',
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `invalidAttempts` INT NOT NULL DEFAULT 0,
  `timestamp` TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

INSERT INTO `securecoding`.`admin`(username,password) VALUES (
  "$2y$10$VFId5DcUQq1aXtYT.szT6u8Sl1bUEkiQTmyrgxawN/j25qwZr/Cx.",
  "$2y$10$.MbjJbD55X0YkDHX0x0EaeCGBJ.YRm9Bsc1Z0UJVo1TR/GcewtDyO"
);

-- -----------------------------------------------------
-- Table `securecoding`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `securecoding`.`client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(60) NOT NULL,
  `username` VARCHAR(15) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `firstName` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `passport` VARCHAR(12) NOT NULL,
  `address` VARCHAR(120) NOT NULL,
  `gender` VARCHAR(45) NOT NULL,
  `status` ENUM('unverified','verified','approved','disabled') NOT NULL DEFAULT 'unverified',
  `verificationKey` CHAR(60) NULL,
  `secret` VARCHAR(50) NOT NULL,
  `scspin` VARCHAR(50) NOT NULL,
  `invalidAttempts` INT NOT NULL DEFAULT 0,
  `timestamp` TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `securecoding`.`employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `securecoding`.`employee` (
  `username` VARCHAR(10) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `firstName` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `invalidAttempts` INT NOT NULL DEFAULT 0,
  `timestamp` TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`username`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `securecoding`.`transactionCode`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `securecoding`.`transactionCode` (
  `client` INT NOT NULL,
  `number` INT NOT NULL,
  `code` VARCHAR(255) NOT NULL,
  INDEX `client_idx` (`client` ASC),
  PRIMARY KEY (`client`, `number`),
  CONSTRAINT `client`
    FOREIGN KEY (`client`)
    REFERENCES `securecoding`.`client` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `securecoding`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `securecoding`.`account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `client` INT NOT NULL,
  `numeration` CHAR(24) NOT NULL,
  `amount` DECIMAL(12,2) NOT NULL,
  `status` ENUM('unconfirmed','confirmed','frozen') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `numeration_UNIQUE` (`numeration` ASC),
  INDEX `fk_account_1_idx` (`client` ASC),
  CONSTRAINT `fk_account_1`
    FOREIGN KEY (`client`)
    REFERENCES `securecoding`.`client` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `securecoding`.`transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `securecoding`.`transaction` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `client` INT NOT NULL,
  `fromAcc` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `toAcc` VARCHAR(34) NOT NULL,
  `amount` DECIMAL(12,2) NOT NULL,
  `status` ENUM('pending','accepted') NOT NULL DEFAULT 'pending',
  `timestamp` TIMESTAMP NOT NULL DEFAULT NOW(),
  `comment` VARCHAR(300) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_transaction_1_idx` (`fromAcc` ASC),
  INDEX `fk_transaction_2_idx` (`client` ASC),
  CONSTRAINT `fk_transaction_1`
    FOREIGN KEY (`fromAcc`)
    REFERENCES `securecoding`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_transaction_2`
    FOREIGN KEY (`client`)
    REFERENCES `securecoding`.`client` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

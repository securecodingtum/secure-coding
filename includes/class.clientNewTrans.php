<?php
class NewTransaction{
	private $from;
	private $to;
	private $name;
	private $amount;
	private $comment;
	private $tan;
	private $requestedTan;
	private $clientId;
	
	
	public function getAccountDetails($username){
		global $db;
		$st = $db->prepare('SELECT a.numeration as accounts FROM account as a, client as b WHERE a.client = b.id and b.username = ?');
		$st->execute(array($username));
		$res = $st->fetchAll();
		
	
		$accounts = array();
		$i=0;
		for($i=0;$i<count($res);$i++){
			$accounts[$i]=$res[$i]['accounts'];
		}
		return $accounts;
	}
	
	public function randomNumberGenerator($username){
		$this->getClientId($username);
		global $db;
		$st = $db->prepare('select number as requestedTan from transactionCode where client = ?');
		$st->execute(array($this->clientId));
		$res = $st->fetchAll();
		$numberOfRows = count($res);
		$randomNumber = mt_rand(0, $numberOfRows-1);
		$requestedTan = $res[$randomNumber]['requestedTan'];
		return $requestedTan;
	}
	
	private function validation() {
		if( !(ctype_alnum($this->from) || ctype_alnum($this->to) || 
				ctype_alnum($this->name) || is_numeric($this->amount) || ctype_digit($this->tan))){
			return false;
		}else{
			return true;
		}		
	}
	
	private function loadInfo(){
		$this->from = $_POST['from'];
		$this->to = $_POST['to'];
		$this->name = $_POST['name'];
		$this->amount = $_POST['amount'];
		$this->comment = $_POST['comment'];
		$this->tan = $_POST['tan'];
		$this->requestedTan = $_POST['requestedTan'];
	}
	
	private  function validate($username){
		if (!$this->validation()){
			return false;
		}else {
			$this->getClientId($username);
			global $db;
			$st = $db->prepare('SELECT count(*) as rows FROM transactionCode as a WHERE a.client = ? and a.code = ? and a.number = ?');
			$st->execute(array($this->clientId,$this->tan,$this->requestedTan));
			$res = $st->fetchAll();
			$rows = $res[0]['rows'];
			if($rows==1){
				return true;
			}else {
				return false;
			}
		}
	}
	
	/**
	 * not used
	 * rather using the default option in DB
	 * @return string
	 */
	private function timestamp(){
		$date = new DateTime();
		return $date->format('d-m-Y H:i:s');
	}
	
	private function getClientId($username){
		global $db;
		$st = $db->prepare('select a.id as clientId from client as a where a.username = ?');
		$st->execute(array($username));
		$res = $st->fetchAll();
		$this->clientId = $res[0]['clientId'];
				
	}
	
	private function getFromAccountId($from){
		global $db;
		$st = $db->prepare('select id as fromAcc from account where numeration = ?');
		$st->execute(array($from));
		$res = $st->fetchAll();
		$from = $res[0]['fromAcc'];
		return $from;
		
	}
	private function insertTransactionDetails(){
		global $db;
		$status=null;
		if($this->amount>10000){
			$status = "pending";
		}else {
			$status = "accepted";
		}
		$from = $this->getFromAccountId($this->from);
		$st = $db->prepare('insert into transaction (client,fromAcc,name,toAcc,amount,status,comment) values (?,?,?,?,?,?,?)');
		$st->execute(array($this->clientId,$from,$this->name,$this->to,$this->amount,$status,$this->comment));
		return $status;
	}
	
	/**
	 * transacation codes should be deleted once user clicks on proceed button.
	 * It does not matters whether user has entered correct or wrong details
	 */
	public function deleteTransactionCode($username){
		$this->getClientId($username);
		global $db;
		$st = $db->prepare('delete from transactionCode where number =? and client =?');
		$st->execute(array($this->requestedTan,$this->clientId));
	}
	
	public function getGender($username){
		global $db;
		$st = $db->prepare('select gender as gender from client where username = ?');
		$st->execute(array($username));
		$res = $st->fetchAll();
		return $res[0]['gender'];
		
	}
	
	public function performTransaction($username){
		$result = null;
		$this->loadInfo();
		$result = $this->validate($username);
		if(!$result){
			return "error";
		}else {
			$result = $this->insertTransactionDetails();
			return $result;
		}
	}
	
}
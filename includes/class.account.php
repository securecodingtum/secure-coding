<?php

class Accounts {

    function get_by_id($id) {
        global $db;
        $st = $db->prepare('SELECT * FROM account WHERE id = ?');
        $st->execute(array($id));
        $res = $st->fetchAll();
        if (sizeof($res) === 0) {
            return null;
        }
        $account = new Account();
        $account->load_info($res[0]);
        return $account;
    }

    private function get_by($what, $value) {
        global $db;
        $st = $db->prepare('SELECT * FROM account WHERE status = ? AND ' . $what . ' = ?');
        $st->execute(array("confirmed", $value));

        $accounts = array();

        foreach ($st->fetchAll() as $row) {
            $account = new Account();
            $account->load_info($row);
            array_push($accounts, $account);
        }

        return $accounts;
    }
    function get_by_status($status) {
        return $this->get_by('status', $status);
    }

    function get_by_client($client) {
        return $this->get_by('client', $client);
    }

    function get_by_numeration($num) {
        return $this->get_by('numeration', $num);
    }

    function get_unconfirmed() {
	global $db;
        $st = $db->prepare("SELECT * FROM account WHERE status = ?");
        $st->execute(array("unconfirmed"));

        $accounts = array();

        foreach ($st->fetchAll() as $row) {
            $account = new Account();
            $account->load_info($row);
            array_push($accounts, $account);
        }

        return $accounts;
    }

	function create($username) {
		global $db;

		$acc = new Account();
		do {
			$acc->numeration = mt_rand(10000000, 99999999);
		} while ($this->exists($acc->numeration));

		$st = $db->prepare('SELECT id FROM client WHERE username = ?');
		$st->execute(array($username));
		$res = $st->fetchAll();

		$acc->client = $res[0][0];
		$acc->amount = 0;
		$acc->status = 'unconfirmed';

		$acc->store();

		return $acc;
	}

    function exists($numeration) {
        global $db;

        $st = $db->prepare('SELECT count(*) as rows FROM account WHERE numeration = ?');
        $st->execute(array($numeration));
        $res = $st->fetchAll()[0];
        $rows = $res["rows"];
        return ($rows == 1);
    }
    
    function validAccount($numeration) {
    	global $db;
    	$status = 'confirmed';
    	$st = $db->prepare('SELECT count(*) as rows FROM account WHERE numeration = ? and status = ?');
    	$st->execute(array($numeration,$status));
    	$res = $st->fetchAll()[0];
    	$rows = $res["rows"];
    	return ($rows == 1);
    }
}

class Account {
    public $id;
    public $client;
    public $numeration;
    public $amount;
    public $status;

    function init($id, $client, $numeration, $amount, $status) {
        $this->id         = $id;
        $this->client     = $client;
        $this->numeration = $numeration;
        $this->amount     = $amount;
        $this->status     = $status;
    }

    function load_info($row) {
        $this->init($row['id'], $row['client'], $row['numeration'], $row['amount'], $row['status']);
    }

    private function exists($numeration) {
        global $db;

        $st = $db->prepare('SELECT count(*) as rows FROM account WHERE numeration = ?');
        $st->execute(array($numeration));
        $res = $st->fetchAll()[0];
        $rows = $res["rows"];

        return ($rows == 1);
    }

    function store() {
        global $db;
        if (!$this->exists($this->numeration)) {
            $st = $db->prepare('INSERT INTO account (client, numeration, amount, status) VALUES (?, ?, ?, ?)');
            $st->execute(array($this->client, $this->numeration, $this->amount, $this->status));
        } else {
            $st = $db->prepare('UPDATE account SET client = ?, numeration = ?, amount = ?, status = ? WHERE id = ?');
            $st->execute(array($this->client, $this->numeration, $this->amount, $this->status, $this->id));
        }
    }

    function delete() {
        global $db;
        if ($this->exists($this->numeration)) {
            $st = $db->prepare('DELETE FROM account WHERE id = ?');
            $st->execute(array($this->id));
        }
    }
    
    function updateAmount($amount, $numeration){
    	global $db;
    	$st = $db->prepare('UPDATE account SET amount = ? WHERE numeration = ?');
    	$st->execute(array($amount, $numeration));
    }
}

?>

<?php

class Transactions {

    function get_by_id($id) {
        global $db;
        $st = $db->prepare('SELECT * FROM transaction WHERE id = ?');
        $st->execute(array($id));
        $res = $st->fetchAll();
        if (sizeof($res) === 0) {
            return null;
        }
        $transaction = new Transaction();
        $transaction->load_info($res[0]);
        return $transaction;
    }

    function get_by_status($status) {
        global $db;
        $st = $db->prepare('SELECT * FROM transaction WHERE status = ?');
        $st->execute(array($status));

        $transactions = array();

        foreach ($st->fetchAll() as $row) {
            $transaction = new Transaction();
            $transaction->load_info($row);
            array_push($transactions, $transaction);
        }

        return $transactions;
    }

    function get_pending()
    {
        return $this->get_by_status('pending');
    }

    function get_by_client($client) {
        global $db;
        $st = $db->prepare('SELECT * FROM transaction WHERE client = ?');
        $st->execute(array($client));

        $transactions = array();

        foreach ($st->fetchAll() as $row) {
            $transaction = new Transaction();
            $transaction->load_info($row);
            array_push($transactions, $transaction);
        }

        return $transactions;
    }

    function get_latest_by_client($client, $limit) {
        global $db;
        $st = $db->prepare('SELECT * FROM transaction WHERE client = :client ORDER BY timestamp DESC LIMIT :limit');
        $st->bindValue(':client', $client);
        $st->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $st->execute();

        $transactions = array();

        foreach ($st->fetchAll() as $row) {
            $transaction = new Transaction();
            $transaction->load_info($row);
            array_push($transactions, $transaction);
        }

        return $transactions;
    }
}

class Transaction {
    public $id;
    public $client;
    public $fromAcc;
    public $toAcc;
    public $amount;
    public $name;
    public $timestamp;
    public $status;
    public $comment;

    function init($id, $client, $fromAcc, $toAcc, $amount, $name, $timestamp, $status, $comment) {
        $this->id = $id;
        $this->client    = $client;
        $this->fromAcc   = $fromAcc;
        $this->toAcc     = $toAcc;
        $this->amount    = $amount;
        $this->name      = $name;
        $this->timestamp = $timestamp;
        $this->status    = $status;
        $this->comment   = $comment;
    }

    function load_info($row) {
        $this->init($row['id'], $row['client'], $row['fromAcc'], $row['toAcc'], $row['amount'],
            $row['name'], $row['timestamp'], $row['status'], $row['comment']);
    }

    function exists($id) {
        global $db;

        $st = $db->prepare('SELECT count(*) as rows FROM transaction WHERE id = ?');
        $st->execute(array($id));
        $res = $st->fetchAll()[0];
        $rows = $res["rows"];

        return ($rows == 1);
    }

    function store() {
        global $db;
        if (!$this->exists($this->id)) {
            $st = $db->prepare('INSERT INTO transaction (client, fromAcc, toAcc, amount, name, timestamp, status, comment) VALUES (?, ?, ?, ?, ?, NOW(), ?, ?)');
            $st->execute(array($this->client, $this->fromAcc, $this->toAcc, $this->amount, $this->name, $this->status, $this->comment));
        } else {
            $st = $db->prepare('UPDATE transaction SET client = ?, fromAcc = ?, toAcc = ?, amount = ?, name = ?, timestamp = ?, status = ?, comment = ? WHERE id = ?');
            $st->execute(array($this->client, $this->fromAcc, $this->toAcc, $this->amount, $this->name, $this->timestamp, $this->status, $this->comment, $this->id));
        }
    }

    function delete() {
        global $db;
        if ($this->exists($this->id)) {
            $st = $db->prepare('DELETE FROM transaction WHERE id = ?');
            $st->execute(array($this->id));
        }
    }
}

?>

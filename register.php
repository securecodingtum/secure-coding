<?php

require_once('includes/db.php');
require_once("includes/class.user.php");
require_once('includes/password.php');
require_once('includes/sanitize.php');
require_once('includes/class.captcha.php');

$captcha = (new Captcha())->init();

$clean = array();

$user = new User();

if (isset($_GET['user']) && isset($_GET['key'])) {
    $user->load_info($_GET['user']);
    if ($user->verify($_GET['key'])) {
        $_GET = sanitize_html($_GET);
        require_once('includes/class.transactionCode.php');
        require_once('includes/class.mail.php');
        require_once('includes/html2pdf_v4.03/html2pdf.class.php');

        $tc = new TransactionCodes();
        $tc->generate_codes($user->id, 100);
        $codes = $tc->get_for_client($user->id);
        $title = "Your transaction numbers";
        $body = "<h2>Welcome to Servus Bank!</h2><p>Please find your transaction numbers (TANs) in the attached PDF. The password for this file is your passport ID.</p>";
        $body .= '<p>';
        $tans = '<h2>Your Servus Bank transaction codes</h2>';
        foreach ($codes as $code) {
            $tans .= '#' . $code['number'] . ': ' . $code['code'] . '<br>';
        }
        $body .= '</p>';
        ob_clean();
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
        $html2pdf->pdf->SetProtection(array('print'), $user->passport);
        $html2pdf->writeHTML($tans);
        $attachment = $html2pdf->Output('','S');
        $mailer = new Mail();
        $mailer->send($title, $body, $user->email, $attachment);
        $secret = $tc->get_scspin($user->username);
        $mailer->send("SCS PIN", "Your SCS PIN: $secret", $user->email, "");
        require('views/postvalidate.php');
    } else {
        $errorMsg = "The specified verification key was wrong.";
        require('views/register.php');
    }
    exit;
} elseif (!isset($_POST['username'])) {
    $validationError = false;
    require('views/register.php');
    exit;
}

$response = $captcha->check();
if (!$response->isValid()) {
    $errorMsg = "CAPTCHA validation failed.";
    require('views/register.php');
    exit;
}

$checks =            ctype_alpha($_POST['firstName'])    && ctype_alpha($_POST['surname']);
$checks = $checks && ctype_alnum($_POST['passport'])     && ctype_alpha($_POST['street']);
$checks = $checks && ctype_digit($_POST['streetNumber']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
$checks = $checks && ctype_digit($_POST['zipcode'])      && ctype_alnum($_POST['username']);
$checks = $checks && (intval($_POST['zipcode'])      == $_POST['zipcode']);
$checks = $checks && (intval($_POST['streetNumber']) == $_POST['streetNumber']);
$checks = $checks && ($_POST['password'] == $_POST['password2']);
$checks = $checks && password_complexity($_POST['password']) && $_POST['password'] != $_POST['username'];
$checks = $checks && ($_POST['gender'] === 'male' || $_POST['gender'] === 'female');

if (!$checks) {
    if (!ctype_digit($_POST['zipcode']) || intval($_POST['zipcode']) != $_POST['zipcode']) {
        $errorMsg = "Sorry. Your ZIP code can only be a number.";
    }
    else if (!ctype_alnum($_POST['username'])) {
        $errorMsg = "Sorry. Your username can only consist of numbers and letters.";
    }
    else if (!ctype_alpha($_POST['firstName']) || !ctype_alpha($_POST['surname'])) {
        $errorMsg = "Sorry. Your name can only consist of letters.";
    }
    else if (!ctype_alnum($_POST['passport'])) {
        $errorMsg = "Sorry. Your passport ID can only consist of numbers and letters.";
    }
    else if (!ctype_alpha($_POST['street'])) {
        $errorMsg = "Sorry. Your street name can only consist of letters.";
    }
    else if (!ctype_digit($_POST['streetNumber']) || intval($_POST['streetNumber']) != $_POST['streetNumber']) {
        $errorMsg = "Sorry. Your street number must be a number.";
    }
    else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errorMsg = "Sorry. The e-mail address you have chosen appears not to be valid.";
    }
    else if ($_POST['password'] != $_POST['password2']) {
        $errorMsg = "Sorry. The introduced passwords do not match.";
    }
    else if (!password_complexity($_POST['password']) || $_POST['password'] == $_POST['username']) {
        $errorMsg = "Sorry. Your password does not meet our complexity requirements.";
    } else {
        $errorMsg = "Sorry. Servus Bank firmly believes in the gender binary.";
    }
    require('views/register.php');
    exit;
}

if (!$user->name_available($_POST['username'])) {
    $errorMsg = "Sorry. This username is taken already.";
    require('views/register.php');
    exit;
}

$validationError = true;

$_POST = sanitize_html($_POST);
$address = $_POST['street'] . " " . $_POST['streetNumber'] . ", in " . $_POST['zipcode'] . " (" . $_POST['country'] . ")";

$user->init($_POST['username'], $_POST['password'], $_POST['passport'], $_POST['email'], $_POST['firstName'], $_POST['surname'], $_POST['gender'], $address);
$user->store();

require_once('includes/class.mail.php');
$title = "Welcome to Servus Bank!";
$body = "<h2>Welcome to Servus Bank!</h2><p>Please click on the following link to verify your e-mail address: ";

$url = '<a href="http://localhost/secure-coding/register.php' . '?user=' . $user->username . '&key=' . $user->verificationKey . '">Verify</a>';
$body .= $url . '</p>';

$to = $_POST['email'];
$mailer = new Mail();
$mailer->send($title, $body, $to, null);


$validationError = false;
require('views/postregister.php');
?>

<?php
session_start();
require_once('includes/db.php');
require_once("includes/class.account.php");
require_once("includes/class.user.php");
require_once("includes/class.transaction.php");

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isUser') {
    header("Location: login.php");
    exit;
}
$user = new User();
$user->load_info($_SESSION['username']);

$accs = new Accounts();
$accounts = $accs->get_by_client($user->id);

$t = new Transactions();
$transactions = $t->get_latest_by_client($user->id, 5);

session_regenerate_id();
require('views/clientMain.php');
?>
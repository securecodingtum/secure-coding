<?php

session_start();

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isAdmin') {
    header("Location: admLogin.php");
    exit;
}

require_once('includes/db.php');
require_once('includes/class.employee.php');
require_once('includes/sanitize.php');
require_once("includes/class.csrf.php");

$success = null;
$error   = null;
$csrf = new Csrf();

if (isset($_POST['username']) && isset($_POST['password']) &&
    isset($_POST['firstName']) && isset($_POST['surname'])) {
    if (!$csrf->verifyToken()) {
        $error = 'Error processing your request.';
    } else {
        $_POST = sanitize_html($_POST);
        $checks = ctype_alpha($_POST['firstName']) && ctype_alpha($_POST['surname']) && ctype_alnum($_POST['username']);
        if ($checks) {
            $emp = new Employee();
            $emp->init($_POST['username'], $_POST['password'], $_POST['firstName'], $_POST['surname']);
            if ($emp->name_available($_POST['username'])) {
                $emp -> store();
                $success = 'Employee ' . $_POST['username'] . ' added successfully.';
            } else {
                $error = 'Another employee with that username exists already.';
            }
        } else {
            if (!ctype_alpha($_POST['firstName']) || !ctype_alpha($_POST['surname'])) {
                $error = 'An employee\'s first name and surname can only consist of letters.';
            } else if (!ctype_alnum($_POST['username'])) {
                $error = 'An employee\'s username can only consist of letters and numbers.';
            }
        }
    }
} else if (isset($_GET['delete'])) {
    if (!$csrf->verifyToken()) {
        $error = 'Error processing your request.';
    } else {
        $_GET = sanitize_html($_GET);
        $emp = new Employee();
        $emp->init($_GET['delete'], null, null, null);
        if (!$emp->name_available($_GET['delete'])) {
            $emp->remove();
            $success = 'Employee ' . $_GET['delete'] . ' deleted successfully.';
        }
    }
}

session_regenerate_id();
$user = new stdClass();
$user->username = 'Mister Admin';
$emp = new Employee();
$employees = $emp->load_all();

$csrf->setToken();
require('views/adm.php');
?>

<?php

require_once('includes/db.php');
require_once("includes/class.employee.php");
require_once("includes/class.user.php");
require_once("includes/class.account.php");
require_once("includes/class.csrf.php");
require_once("includes/sanitize.php");

if(!isset($_SESSION))
	session_start();
$error = null;
if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isEmployee') {
	$_SESSION['from'] = "employee.php";
	header("Location: empLogin.php");
	exit;
}
$user = new Employee();
$csrf = new Csrf();
$user->load_info($_SESSION['username']);

if(isset($_POST['account']) && isset($_POST['amount'])){
	if ($csrf->verifyToken()){
		$_POST = sanitize_html($_POST);
		$amount = $_POST['amount'];
		$account = $_POST['account'];
		if(validations($amount, $account)){
			$acc = new Account();
			$acc->updateAmount($amount, $account);
			$success = 'Account balance successfully set.';
		} else {
			$error = 'The specified data are not valid. Please try again.';
		}
	}else {
		$error = 'Error processing your request.';
	}
}

function validations($amount,$account){
	if(strlen($account)>24 ||
			strlen($amount)>12 ||
			!is_numeric($amount) ||
			!validDecimalInput($amount) ||
			$amount<0){
		return false;
	}else {
		return validAccount($account);
	}
}

function validDecimalInput($amount){
	$decimal = explode('.', $amount);
	if(count($decimal)==2 && strlen($decimal[1])>2)
		return false;
	else
		return true;
}

function validAccount($account){
	$accs = new Accounts();
	return $accs->exists($account);
}

if(isset($_POST['userId'])){
	$_POST = sanitize_html($_POST);
	$accs = new Accounts();
	$userId = $_POST['userId'];
	$accounts = $accs->get_by_client($userId);
}
session_regenerate_id();
$csrf->setToken();
require 'views/cashDeposit.php';
?>
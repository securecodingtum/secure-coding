<?php

if(!isset($_SESSION))
	session_start();
require_once('includes/db.php');
require_once("includes/class.account.php");
require_once("includes/class.transaction.php");
require_once("includes/class.user.php");

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || ($_SESSION['roleCheckFlag'] != 'isUser' && $_SESSION['roleCheckFlag'] != 'isEmployee')) {
	header("Location: login.php");
	exit;
}
if(isset($_SESSION['roleCheckFlag']) && $_SESSION['roleCheckFlag'] == 'isUser'){
	$user = new User();
	$user->load_info($_SESSION['username']);
}


$accs = new Accounts();
$accountNames = array();
$accounts = $accs->get_by_client($user->id);
foreach ($accounts as $account) {
	$accountNames[$account->id] = $account->numeration;
}
$requestStatus = null;

$t = new Transactions();

$transactions = $t->get_by_client($user->id);

session_regenerate_id();
require('views/clientViewTrans.php');
?>

#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <mysql.h>
#include <errno.h>

#define DEBUG

#ifdef DEBUG
#define DPRINTF(...)	printf(__VA_ARGS__)
#else
#define DPRINTF(...)
#endif

int userid;

struct _transaction {
	int client;
	int fromAcc;
	char *name;
	int toAcc;
	char *fromNum;
	char *toNum;
	float amount;
	enum _status { pending, accepted } status;
	char *comment;
};

typedef struct _transaction transaction;

int insert_transaction(MYSQL *, transaction *);
int get_from_numeration(MYSQL *, char *);
int update_amount(MYSQL *, float, int);
int valid_account(MYSQL *, int, int);
void sanitize_transaction(transaction *);
int check_amount(MYSQL *, int, float);


/*
 * file format: from,to,name,amount,comment
 */
int
parse(MYSQL *conn, FILE *fd)
{
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
	transaction t;
	int i;

	for (i = 1; (read = getline(&line, &len, fd)) != -1; i++) {
		int n = sscanf(line, "%m[^,],%m[^,],%m[^,],%f,%m[^,]",
			&t.fromNum, &t.toNum, &t.name, &t.amount, &t.comment);
		if (n == 5) {
			sanitize_transaction(&t);

			if (t.amount <= 0) {
				DPRINTF("%02d: negative amounts of money (%.2f) are not allowed\n", i, t.amount);
				continue;
			}
			if (t.amount >= 10000) {
				t.status = pending;
			} else {
				t.status = accepted;
			}

			t.client = userid;
			if (t.client < 0) {
				DPRINTF("%02d: there is no client with name '%s'\n", i, t.name);
				continue;
			}

			t.fromAcc = get_from_numeration(conn, t.fromNum);
			t.toAcc = get_from_numeration(conn, t.toNum);
			if (t.toAcc == t.fromAcc) {
				DPRINTF("%02d: the origin and destination accounts are the same.\n", i);
				continue;
			}

			if (!valid_account(conn, t.fromAcc, userid)) {
				DPRINTF("%02d: the origin account is incorrect.\n", i);
				continue;
			}

			if (check_amount(conn, t.fromAcc, t.amount) != 1) {
				DPRINTF("%02d: the account does not contain enough money.\n", i);
				continue;
			}

			if (insert_transaction(conn, &t) < 0) {
				continue;
			}

			if (t.status == pending) {
				DPRINTF("%02d: transaction waiting for approval.\n", i);
			} else {
				DPRINTF("%02d: transaction performed successfully.\n", i);
			}
		} else if (errno != 0) {
			perror("sscanf");
			exit(1);
		} else {
			DPRINTF("%02d: line is not correctly formatted\n", i);
		}

		free(t.name);
		free(t.fromNum);
		free(t.toNum);
		free(t.comment);
	}
	free(line);

	return 0;
}

int
update_amount(MYSQL *conn, float amount, int account)
{
	char *sql;
	int n;

	n = asprintf(&sql, "update account set amount = amount + %f where id = %d", amount, account);
	if (n < 0) {
		perror("asprintf");
		return -1;
	}

	if (mysql_query(conn, sql) != 0) {
		DPRINTF("mysql: %s, query: %s\n", mysql_error(conn), sql);
		return -1;
	}
	free(sql);

	if (mysql_affected_rows(conn) < 0) {
		// XXX: error
		return -1;
	}

	return 0;
}

void
sanitize_transaction(transaction *t)
{
	char *esc_name = malloc(strlen(t->name)*2+1);
	char *esc_comment = malloc(strlen(t->comment)*2+1);

	if (!esc_name || !esc_comment) {
		perror("malloc");
		exit(1);
	}

	mysql_escape_string(esc_name, t->name, strlen(t->name));
	mysql_escape_string(esc_comment, t->comment, strlen(t->comment));

	free(t->name);
	free(t->comment);

	t->name = esc_name;
	t->comment = esc_comment;
}

int
insert_transaction(MYSQL *conn, transaction *t)
{
	char *sql;
	int n;

	char *status = (t->status == 0) ? "pending" : "accepted";

	n = asprintf(&sql, "insert into transaction (client, fromAcc, name, toAcc, amount, comment, status) values ('%d', '%d', '%s', '%s', '%f', '%s', '%s')",
		t->client, t->fromAcc, t->name, t->toNum, t->amount, t->comment, status);
	if (n < 0) {
		perror("asprintf");
		return -1;
	}

	if (mysql_query(conn, sql) != 0) {
		DPRINTF("mysql: %s, query: %s\n", mysql_error(conn), sql);
		return -1;
	}
	free(sql);

	if (mysql_affected_rows(conn) < 0) {
		return -1;
	}

	if (t->status == 0) {
		/* we are waiting for approval */
		return 0;
	}

	/* subtract money from sender's account */
	if (update_amount(conn, -(t->amount), t->fromAcc) < 0) {
		return -1;
	}

	/* add money to the receiver */
	if (update_amount(conn, t->amount, t->toAcc) < 0) {
		/* we should rollback the subtraction, but it's either a memory
		* or connection error so we won't be able to fix it anyway */
		return -1;
	}
	return 0;
}

int
valid_account(MYSQL *conn, int account, int userid)
{
	char *sql;
	int n, client;
	MYSQL_RES *res;
	MYSQL_ROW row;
	n = asprintf(&sql, "select client from account where id = '%d'", account);
	if (n < 0) {
		perror("asprintf");
		return -1;
	}
	if (mysql_query(conn, sql) != 0) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		DPRINTF("query: %s\n", sql);
		return -1;
	}
	free(sql);
	res = mysql_use_result(conn);
	if (res == NULL) {
		DPRINTF("%s\n", mysql_error(conn));
		return -1;
	}
	row = mysql_fetch_row(res);
	if (row == NULL) {
		mysql_free_result(res);
		DPRINTF("%s\n", mysql_error(conn));
		return -1;
	}

	client = atoi(row[0]);
	mysql_free_result(res);

	return (userid == client) ? 1 : 0;
}

int
check_amount(MYSQL *conn, int account, float amount)
{
	char *sql;
	int n;
	float a;

	MYSQL_RES *res;
	MYSQL_ROW row;
	n = asprintf(&sql, "select amount from account where id = '%d'", account);
	if (n < 0) {
		perror("asprintf");
		return -1;
	}
	if (mysql_query(conn, sql) != 0) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		DPRINTF("query: %s\n", sql);
		return -1;
	}
	free(sql);
	res = mysql_use_result(conn);
	if (res == NULL) {
		DPRINTF("%s\n", mysql_error(conn));
		return -1;
	}
	row = mysql_fetch_row(res);
	if (row == NULL) {
		mysql_free_result(res);
		DPRINTF("%s\n", mysql_error(conn));
		return -1;
	}

	a = atof(row[0]);
	mysql_free_result(res);

	return amount <= a;
}

int
get_from_numeration(MYSQL *conn, char *numeration)
{
	char *sql;
	int n, id;
	MYSQL_RES *res;
	MYSQL_ROW row;
	n = asprintf(&sql, "select id from account where numeration = '%s'", numeration);
	if (n < 0) {
		perror("asprintf");
		return -1;
	}
	if (mysql_query(conn, sql) != 0) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		DPRINTF("query: %s\n", sql);
		return -1;
	}
	free(sql);
	res = mysql_use_result(conn);
	if (res == NULL) {
		DPRINTF("%s\n", mysql_error(conn));
		return -1;
	}
	row = mysql_fetch_row(res);
	if (row == NULL) {
		mysql_free_result(res);
		DPRINTF("%s\n", mysql_error(conn));
		return -1;
	}

	id = atoi(row[0]);
	mysql_free_result(res);

	return id;
}

void
usage(char *name)
{
	fprintf(stderr, "Usage: %s [-h host] [-u username] [-p password] [-d database] file userid\n", name);
	exit(1);
}

/*
 * XXX: it's not checked if fromAcc belongs to the client executing the transaction
 */
int
main(int argc, char *argv[])
{
	char *path;
	char *host = "localhost";
	char *user = "root";
	char *pass = "blamuhblamuh";
	char *database = "securecoding";
	FILE *fd;
	int opt;

	if (argc < 3)
		usage(argv[0]);

	while ((opt = getopt(argc, argv, "h:u:p:d:")) != -1) {
		switch (opt) {
		case 'h':
			host = optarg;
			break;
		case 'u':
			user = optarg;
			break;
		case 'p':
			pass = optarg;
			break;
		case 'd':
			database = optarg;
			break;
		default:
			usage(argv[0]);
			break;
		}
	}
	if (optind >= argc) {
		fprintf(stderr, "Expected argument after options\n");
		exit(1);
	}
	path = argv[optind];
	userid = atoi(argv[optind+1]);

	fd = fopen(path, "r");
	if (!fd) {
		perror("fopen");
		exit(1);
	}

	MYSQL *conn;
	conn = mysql_init(NULL);
	if (conn == NULL) {
		DPRINTF("mysql_init: not enough memory\n");
		exit(1);
	}
	/* Connect to database */
	if (!mysql_real_connect(conn, host, user, pass, database, 0, NULL, 0)) {
		DPRINTF("%s\n", mysql_error(conn));
		exit(1);
	}

	// XXX
	if (parse(conn, fd) < 0) {
		// XXX: error
		exit(1);
	}

	mysql_close(conn);
	fclose(fd);
	return 0;
}
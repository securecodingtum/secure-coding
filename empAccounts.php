<?php

require_once('includes/db.php');
require_once("includes/class.employee.php");
require_once("includes/class.account.php");
require_once("includes/class.user.php");
require_once('includes/sanitize.php');
require_once("includes/class.csrf.php");

session_start();

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isEmployee') {
    $_SESSION['from'] = "employee.php";
    header("Location: empLogin.php");
    exit;
}
$user = new Employee();
$user->load_info($_SESSION['username']);

$success = null;
$acc = new Accounts();
$csrf = new Csrf();
if (isset($_GET['type']) && isset($_GET['id'])) {
	if(!$csrf->verifyToken()){
				$success = 'Error processing request.';
	}else{
		$_GET = sanitize_html($_GET);

		$editedAcc = $acc->get_by_id($_GET['id']);
		if ($editedAcc !== null) {
			if ($_GET['type'] === 'accept') {
				$editedAcc->status = 'confirmed';
				$editedAcc->store();
				$success = 'Account accepted successfully.';
			} else if ($_GET['type'] === 'reject') {
				$editedAcc->delete();
				$success = 'Account rejected successfully.';
			}
		}
		$csrf->setToken();
	}
}

$accounts = $acc->get_unconfirmed();
$clients = array();
$users = new Users();
foreach ($accounts as $account) {
    if (!isset($clients[$account->client])) {
        $client = $users->get_by_id($account->client);
        $clients[$account->client] = $client->surname . ", " . $client->name;
    }
}
session_regenerate_id();
require('views/empAccounts.php');
?>

<?php

session_start();
require_once('includes/db.php');
require_once("includes/class.user.php");
require_once('includes/sanitize.php');
require_once("includes/class.csrf.php");
require_once('includes/class.captcha.php');

ini_set('session.use_only_cookies', true);
$captcha = (new Captcha())->init();
$csrf = new Csrf();

if (isset($_POST['username']) && isset($_POST['password'])) {
    $user = new User();
    $csrf = new Csrf();
    $_POST = sanitize_html($_POST);
    $user->load_info($_POST['username']);
	$timestamp = date('Y-m-d G:i:s');

    if ($captcha->check()->isValid()) {
		if( $user->login($_POST['password'])){
			if($user->getInvalidAttempts() >=3 && time()-strtotime($user->getTimestamp()) < 3600){
				$error = "Account is locked. Attempt after sometime";
				require('views/login.php');
				exit;
			}elseif($user->getInvalidAttempts()>0){
				$user->updateLoginAttempts(0,$timestamp,$user->username);
			}
			if ($user->status === 'approved') {
				$_SESSION['username'] = $_POST['username'];
				//$_SESSION['isUser']   = true;
				$_SESSION['roleCheckFlag'] = 'isUser';
				header("Location: clientMain.php");
				exit;
			}else {
            if ($user->status === 'unverified') {
                $error = "Please verify your e-mail address before trying to log into your account.";
            } else if ($user->status === 'verified') {
                $error = "Your account request is currently under review by a Servus Bank employee.";
            } else {
                $error = "Your account is currently unavailable. Please contact our staff for more information.";
            }
				require('views/login.php');
				exit;
			}
        }else {
			$user->updateLoginAttempts($user->getInvalidAttempts()+1,$timestamp);
			$error = "Username or password were wrong.";
			require('views/login.php');
			exit;
			}
    } else {
        $error = "CAPTCHA validation failed.";
        require('views/login.php');
        exit;
    }
}

if (isset($_GET['logout'])) {
    unset($_SESSION['username']);
    //unset($_SESSION['isUser']);
	unset($_SESSION['roleCheckFlag']);
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }
    session_destroy();
    $error = "You were successfully logged out.";
    require('views/login.php');
    exit;
}

$csrf->setToken();
require('views/login.php');
?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            <ul id="menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Products</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <a href="login.php" id="clientLogin">Client login</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <div class="content">
        <img class="floatRight" src="img/placeholder.png">
        <h2>Registration complete!</h2>
        <p>Your e-mail address has been verified successfully. You will be able to access the client area in a few
        hours. We will send you an e-mail message when your personal area is ready.</p>
        <p>Welcome to Servus Bank!</p>
        <div class="clear"></div>
        <div class="footer">
            <a href="#">Legal information</a> ·
            <a href="#">Rates</a> ·
            <a href="#">Site map</a> ·
            <a href="empLogin.php">Employees</a>
        </div>
    </div>
</div>

</body>
</html>
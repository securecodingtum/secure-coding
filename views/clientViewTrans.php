<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="css/jquery.datatables.css" rel="stylesheet" type="text/css">
<title>Servus Bank</title>
<script src="js/jquery-2.1.1.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.dataTables.js"></script>
<script>
        $(function() {
            $('table').dataTable();
            $('button').button();
        });
    </script>
</head>
<body>

	<div id="headerWrapper">
		<div id="header">
			<h2>
				<img src="img/logoSmall.png"> Servus Bank
			</h2>
			<div id="menuContainer">
				You are logged in as: <span class="user"><?php
				if ($user->gender === 'male') {
					echo 'Mr. ';
				} else {
					echo 'Ms. ';
				}
				echo $user->name . ' ' . $user->surname;
				?> </span>. <a href="login.php?logout=1">Log out</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<div id="mainContainer">
		<div class="content">

			<h2>Transactions</h2>

			<?php if (sizeof($transactions) > 0) {
				echo '<table>
				<thead>
				<tr>
				<th>Date and time</th><th>From</th><th>To</th><th>Name</th><th>Amount</th><th>Status</th><th>Comment</th>
				</tr>
				</thead>
				<tbody>';

				foreach ($transactions as $transaction) {
					echo '<tr>';
					echo "<td>" . $transaction->timestamp . "</td>";
					echo "<td>" . $accountNames[$transaction->fromAcc] . "</td>";
					echo "<td>" . $transaction->toAcc . "</td>";
					echo "<td>" . $transaction->name . "</td>";
					echo "<td>" . $transaction->amount . "</td>";
					echo '<td>' . (($transaction->status === 'accepted') ? 'Accepted' : 'Pending') . '</td>';
					echo "<td>" . $transaction->comment . "</td>";
					echo "</tr>\n";
				}
				echo '</tbody></table>';
				if(isset($_SESSION['roleCheckFlag']) && $_SESSION['roleCheckFlag'] == 'isUser')
					echo '<p><a href="downloadTransactions.php"><button>Download transaction history</button></a></p>';
			} else {
				echo '<p>You do not have any transactions yet.</p>';
			}
			?>
			<?php 
			if(isset($_SESSION['roleCheckFlag']) && $_SESSION['roleCheckFlag'] == 'isEmployee'){
				echo '<p><a href="employee.php"><button>Back to employee area</button></a></p>';
			}elseif (isset($_SESSION['roleCheckFlag']) && $_SESSION['roleCheckFlag'] == 'isUser'){
				echo '<p><a href="clientMain.php"><button>Back to client area</button></a></p>';
			}
			?>

			<input type="hidden" name="token"
				value="<?php echo $_SESSION['token_value']; ?>" />
		</div>
	</div>

</body>
</html>

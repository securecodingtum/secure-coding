<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            <ul id="menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Transactions</a></li>
            </ul>
            <a href="login.php?logout=1" id="clientLogin">Logout</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <div class="content">
        <img class="floatRight" src="img/placeholder.png">
	<?php
	if (isset($error)) {
		echo '<p style="color:red">' . $error . '</p>';
	}
	?>

	<p>foo bar baz</p>
    </div>
</div>

</body>
</html>
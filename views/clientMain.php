<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( " button" ).button();
        });
    </script>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            You are logged in as: <span class="user"><?php
                        if ($user->gender === 'male') {
                                echo 'Mr. ';
                        } else {
                            echo 'Ms. ';
                        }
                echo $user->name . ' ' . $user->surname;
                        ?></span>. <a href="login.php?logout=1">Log out</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <div class="content">

        <h2>Client area</h2>

        <a href="downloadScs.php" target="_blank"><button>Download your SCS</button></a>

        <h3 class="separate">Bank accounts</h3>
        <?php
        if (sizeof($accounts) > 0) {
            echo '<table>
                <tr><th>Account</th><th>Balance</th></tr>';
            foreach ($accounts as $a) {
                echo '<tr><td>' . $a->numeration . '</td><td>' . $a->amount . '</td></tr>';
            }
            echo ' </table>';
        } else {
            echo '<p>You do not have any accounts yet.</p>';
        }
        ?>

        <a href="clientAccounts.php"><button>Manage my bank accounts</button></a>

        <h3 class="separate">Transactions</h3>

        <?php
        if (sizeof($transactions) > 0) {
            echo '<table>
            <thead>
                <tr>
                   <th>Name</th><th>Amount</th><th>Status</th>
                </tr>
            </thead>
            <tbody>';
            foreach ($transactions as $transaction) {
                echo '<tr>';
                echo "<td>" . $transaction->name . "</td>";
                echo "<td>" . $transaction->amount . "</td>";
                echo '<td>' . (($transaction->status === 'accepted') ? 'Accepted' : 'Pending') . '</td>';
                echo "</tr>\n";
            }
            echo '</tbody></table>';
        } else {
            echo '<p>You have not performed nor received any transactions yet.</p>';
        }
        ?>

        <a href="clientViewTrans.php"><button>View all transactions</button></a>
        <a href="clientNewTrans.php"><button>Make a new transaction</button></a>
        <input type="hidden" name="token" value="<?php echo $_SESSION['token_value']; ?>" />
    </div>
</div>

</body>
</html>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script>
         $(function() {
            $( "input[type=submit]" ).button();
        });
    </script>
</head>
<body>

	<div id="headerWrapper">
		<div id="header">
			<h2>
				<img src="img/logoSmall.png"> Servus Bank
			</h2>
			<div id="menuContainer">
				You are logged in as: <span class="user"><?php echo $user->username; ?>
				</span>. <a href="empLogin.php?logout=logout">Log out</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<div id="mainContainer">
		<div class="content">
			<h2>Control panel</h2>
			<br class="separate">

			<h3>Client revisions</h3>
			<p>
				<?php if ($numClients === 0) {
					echo 'There are no new clients pending review.';
				} else {
					echo 'There are <a href="empClients.php">' . $numClients . ' new clients</a> pending review.';
            }?>
			</p>
			<br class="separate">

			<h3 class="separate">Transactions</h3>
			<p>
				<?php if ($numTransactions === 0) {
					echo 'There are no transactions pending review.';
				} else {
					echo 'There are <a href="empTransactions.php">' . $numTransactions . ' transactions</a> pending review.';
            }?>
			</p>

			<br class="separate">
			<h3 class="separate">Bank account requests</h3>
			<p>
				<?php if ($numAccounts === 0) {
					echo 'There are no bank account requests pending review.';
				} else {
					echo 'There are <a href="empAccounts.php">' . $numAccounts . ' bank account requests</a> pending review.';
            }?>
			</p>

			<br class="separate">
			<h3 class="separate">Client management</h3>
			<?php
			if (isset($success)) {
				echo '<p>' . $success . '</p>';
			}
			else if (isset($error)) {
				echo '<p class="error">' . $error . '</p>';
			}
			?>
			<p>You can manage a specific client by indicating the client's
				username:</p>
			<form action="empClientManagement.php" method="post">
				<div class="formRow">
					<label for="username">Username:</label> <input type="text"
						name="username" id="username" maxlength="15" required>
				</div>
				<input type="submit" name="viewTrans" id="submit"
					value="View client transactions"> <input type="submit"
					name="overrideBalance" id="submit"
					value="Override account balances"> <input type="hidden"
					name="token" value="<?php echo $_SESSION['token_value']; ?>" />
			</form>
		</div>
	</div>

</body>
</html>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script>$(function() {
            $( " input[type=submit]" ).button();
        });

        var passwordIsStrong = function() {
            var password = $('#password').val();
            if (password.length < 8 || password == $('#username').val()) {
                return false;
            }
            var numbers   = 0;
            var lowercase = 0;
            var uppercase = 0;
            var symbols   = 0;
            for (var i = 0; i < password.length; i++) {
                if ('0123456789'.indexOf(password.charAt(i)) > -1) {
                    numbers++;
                } else if ('abcdefghijklmnopqrstuvwxyz'.indexOf(password.charAt(i)) > -1) {
                    lowercase++;
                } else if ('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.indexOf(password.charAt(i)) > -1) {
                    uppercase++;
                } else {
                    symbols++;
                }
            }

            var variety = 0;
            if (numbers   > 0) variety++
            if (lowercase > 0) variety++;
            if (uppercase > 0) variety++;
            if (symbols   > 0) variety++;

            if (variety > 2) {
                return true;
            } else {
                return false;
            }
        };

        var passwordsMatch = function() {
            return $('#password').val() == $('#password2').val();
        }

        var validatePasswords = function() {
            var showNoStrength = function() {
                $('#password').addClass('wrongPassword');
                $('#password2').removeClass('wrongPassword');
                $('#pwMismatch').hide();
                $('#weakPassword').addClass('error');
                $('html, body').animate({
                    scrollTop: $("#password").offset().top
                }, 200);
            }
            var showMismatch = function() {
                $('#password').addClass('wrongPassword');
                $('#password2').addClass('wrongPassword');
                $('#pwMismatch').show();
                $('#weakPassword').removeClass('error');
                $('html, body').animate({
                    scrollTop: $("#password").offset().top
                }, 200);
            }

            if (passwordIsStrong()) {
                if (passwordsMatch()) {
                    return true;
                } else {
                    showMismatch();
                }
            } else {
                showNoStrength();
            }
            return false;
        }
    </script>
    <title>Servus Bank</title>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            <ul id="menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Products</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <a href="login.php" id="clientLogin">Client login</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <div class="content">
        <img class="floatRight" src="img/placeholder.png">
        <h2>Become a client</h2>
        <p>You are about to become a client of Servus Bank, TUM's leader in reasonably secure online banking.
           Please complete the following fields with your information.</p>
        <?php
            if (isset($validationError) && $validationError) {
                echo '<p class="error">There has been an error. Please check your input.</p>';
            }
            if (isset($errorMsg)) {
                echo '<p class="error">' . $errorMsg . '</p>';
            }
        ?>
        <form action="register.php" method="post" onsubmit="return validatePasswords()">
            <div class="formRow">
                <label for="firstName">First name:</label>
                <input type="text" maxlength="45" id="firstName" name="firstName" required>
            </div>
            <div class="formRow">
                <label for="surname">Surname:</label>
                <input type="text" maxlength="45" id="surname" name="surname" required>
            </div>
            <div class="formRow">
                <label for="passport">Passport ID:</label>
                <input type="text" maxlength="12" id="passport" name="passport" required>
            </div>
            <div class="formRow">
                <label for="gender">Gender:</label>
                <select id="gender" name="gender">
                    <option value="female">Female</option>
                    <option value="male">Male</option>
                </select>
            </div>
            <div class="formRow">
                <label for="email">E-mail:</label>
                <input type="text" maxlength="60" id="email" name="email" required>
            </div>
            <div class="formRow">
                <label for="street">Street:</label>
                <input type="text" maxlength="60" id="street" name="street" required>
            </div>
            <div class="formRow">
                <label for="streetNumber">Number:</label>
                <input type="number" name="streetNumber" id="streetNumber" required>
            </div>
            <div class="formRow">
                <label for="zipcode">ZIP code:</label>
                <input type="number" name="zipcode" id="zipcode" required>
            </div>
            <div class="formRow">
                <label for="country">Country:</label>
                <select id="country" name="country">
                    <option value="be">Belgium</option>
                    <option value="it">Italy</option>
                    <option value="fr">France</option>
                    <option value="de" selected="selected">Germany</option>
                    <option value="nl">Netherlands</option>
                    <option value="pt">Portugal</option>
                    <option value="es">Spain</option>
                    <option value="ch">Switzerland</option>
                    <option value="uk">United Kingdom</option>
                </select>
            </div>

            <hr class="clear">

            <div class="formRow">
                <label for="username">Desired username:</label>
                <input type="text" maxlength="15" name="username" id="username" required>
            </div>
            <p>This is the username which you will use to log into your Servus Bank client area.
               It needs to consist of between 5 and 15 numbers and letters.</p>

            <div class="formRow">
                <label for="password">Password:</label>
                <input type="password" maxlength="20" name="password" id="password" required>
            </div>

            <p id="weakPassword">Your password for Servus Bank needs to consist of between 8 and 20 characters.
                Your password needs to contain three of the following elements: lowercase letters, uppercase letters,
                symbols and numbers. Additionally, it must not be the same as your username.</p>
            <div class="formRow">
                <label for="password2">Confirm password:</label>
                <input type="password" maxlength="20" name="password2" id="password2" required>
            </div>
            <p class="error" style="display: none" id="pwMismatch">Sorry. The introduced passwords do not match.</p>

            <hr>

            <h3>Terms and conditions</h3>

            <textarea cols="114" rows="20" disabled="disabled">Driftwood (struggling to read the fine print): I can read but I can't see it. I don't seem to have it in focus here. If my arms were a little longer, I could read it. You haven't got a baboon in your pocket, have ya? Here, here, here we are. Now I've got it. Now pay particular attention to this first clause because it's most important. It says the, uh, "The party of the first part shall be known in this contract as the party of the first part." How do you like that? That's pretty neat, eh?
Fiorello: No, it's no good.
Driftwood: What's the matter with it?
Fiorello: I don't know. Let's hear it again.
Driftwood: It says the, uh, "The party of the first part shall be known in this contract as the party of the first part."
Fiorello: (pausing) That sounds a little better this time.
Driftwood: Well, it grows on ya. Would you like to hear it once more?
Fiorello: Uh, just the first part.
Driftwood: What do you mean? The party of the first part?
Fiorello: No, the first part of the party of the first part.
Driftwood: All right. It says the, uh, "The first part of the party of the first part shall be known in this contract as the first part of the party of the first part shall be known in this contract" - look, why should we quarrel about a thing like this? We'll take it right out, eh?
Fiorello: Yeah, it's a too long, anyhow. (They both tear off the tops of their contracts.) Now, what do we got left?
Driftwood: Well, I got about a foot and a half. Now, it says, uh, "The party of the second part shall be known in this contract as the party of the second part."
Fiorello: Well, I don't know about that...
Driftwood: Now what's the matter?
Fiorello: I no like-a the second party, either.
Driftwood: Well, you should've come to the first party. We didn't get home 'til around four in the morning... I was blind for three days!
Fiorello: Hey, look, why can'ta the first part of the second party be the second part of the first party? Then a you gotta something.
Driftwood: Well, look, uh, rather than go through all that again, what do you say?
Fiorello: Fine. (They rip out a portion of the contract.)
Driftwood: Now, uh, now I've got something you're bound to like. You'll be crazy about it.
Fiorello: No, I don't like it.
Driftwood: You don't like what?
Fiorello: Whatever it is. I don't like it.
Driftwood: Well, don't let's break up an old friendship over a thing like that. Ready?...
Fiorello: OK! (Another part is torn off.) Now the next part, I don't think you're gonna like.
Driftwood: Well, your word's good enough for me. (They rip out another part.) Now then, is my word good enough for you?
Fiorello: I should say not.
Driftwood: Well, that takes out two more clauses. (They rip out two more parts.) Now, "The party of the eighth part..."
Fiorello: No, that'sa no good. (more ripping.) No.
Driftwood: "The party of the ninth part..."
Fiorello: No, that'sa no good, too. (they rip the contracts again until there's practically nothing left.) Hey, how is it my contract is skinnier than yours?
Driftwood: Well, I don't know. You must've been out on a tear last night. But anyhow we're all set now, aren't we?
Fiorello: Oh sure.
Driftwood (offering his pen to sign the contract): Now just, uh, just you put your name right down there and then the deal is, uh, legal.
Fiorello: I forgot to tell you. I can't write.
Driftwood: Well, that's all right, there's no ink in the pen anyhow. But listen, it's a contract, isn't it?
Fiorello: Oh sure.
Driftwood: We got a contract...
Fiorello: You bet.
Driftwood: No matter how small it is...
Fiorello: Hey, wait, wait. What does this say here? This thing here.
Driftwood: Oh, that? Oh, that's the usual clause. That's in every contract. That just says uh, it says uh, "If any of the parties participating in this contract is shown not to be in their right mind, the entire agreement is automatically nullified."
Fiorello: Well, I don't know...
Driftwood: It's all right, that's, that's in every contract. That's, that's what they call a 'sanity clause'.
Fiorello: Ha ha ha ha ha! You can't fool me! There ain't no Sanity Clause!
Driftwood: Well, you win the white carnation.
Fiorello: I give this to Riccardo.

(Extracted from "A Night at the Opera", 1935)</textarea>

            <br class="clear">

            <div class="formRow checkbox">
                <input type="checkbox" name="accepted" id="accepted" required>
                <label for="accepted">I have read and accepted the Terms and conditions.</label>
            </div>

            <hr>

            <h3>Security check</h3>

            <p>Please write in the characters in the following image:</p>

            <?php echo $captcha->html(); ?>

            <input type="submit" value="Register">

        </form>
        <div class="footer">
            <a href="#">Legal information</a> ·
            <a href="#">Rates</a> ·
            <a href="#">Site map</a> ·
            <a href="empLogin.php">Employees</a>
        </div>
    </div>
</div>

</body>
</html>

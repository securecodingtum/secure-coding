<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script>
         $(function() {
            $( "input[type=submit]" ).button();
            $( "button" ).button();
        });
    </script>
</head>
<body>

	<div id="headerWrapper">
		<div id="header">
			<h2>
				<img src="img/logoSmall.png"> Servus Bank
			</h2>
			<div class="clear"></div>
		</div>
	</div>

	<div id="mainContainer">

		<div class="content">
			<h2>Password recovery</h2>

			<?php

            if (isset($success)) {
                echo '<p>' . $success . '</p>';
            } else {
                if (isset($error)) {
                    echo '<p class="error">' . $error . '</p>';
                }

                echo '<p>Please fill in the following fields:</p>
                    <form action="passwordRecovery.php" method="post">
                        <div class="formRow">
                            <label for="username">Username:</label> <input type="text"
                                maxlength="15" name="username" id="username" required>
                        </div>
                        <div class="formRow">
                            <label for="passport">Passport ID:</label> <input type="text"
                                maxlength="12" id="passport" name="passport" required>
                        </div>
                        <div class="formRow">
                            <label for="email">E-mail:</label> <input type="text"
                                maxlength="60" id="email" name="email" required>
                        </div>
                        <input type="submit" class="separate" value="Change password">
                        <input type="hidden" name="token" value="'.$_SESSION['token_value'].'" />
                    </form>';
            }

            ?>
			<br class="separate"> <a href="login.php"><button>Back to client area</button></a>
		</div>
	</div>

</body>
</html>

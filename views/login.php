<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/clearPassword.js"></script>
    <script>
         $(function() {
            $( "input[type=submit]" ).button();
        });
    </script>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            <ul id="menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Products</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <a href="login.php" id="clientLogin">Client login</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <div class="content">
        <img class="floatRight" src="img/placeholder.png">
        <h2>Log in</h2>
	<?php
	if (isset($error)) {
		echo '<p class="error">' . $error . '</p>';
	}
	?>
        <p>To access the client area and manage your accounts, please enter your log in details:</p>
        <form action="login.php" method="post" autocomplete="off">
            <div class="formRow small">
                <label for="username">Username:</label>
                <input type="text" maxlength="15" name="username" id="username">
            </div>
            <div class="formRow small">
                <label for="password">Password:</label>
                <input type="password" maxlength="20" name="password" id="password">
            </div>
            <p>Please write in the characters in the following image:</p>
            <?php echo $captcha->html(); ?>
            <input type="submit" class="separate" value="Log in">
            <a href="passwordRecovery.php">Forgot password</a>
        </form>
        <div class="clear"></div>
        <div class="footer">
            <a href="#">Legal information</a> ·
            <a href="#">Rates</a> ·
            <a href="#">Site map</a> ·
            <a href="empLogin.php">Employees</a>
        </div>
    </div>
</div>

</body>
</html>

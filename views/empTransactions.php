<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#accordion" ).accordion({
                collapsible: true
            });
            $('button').button();
        });
        var run = function(id, token, type) {
            if (confirm('Are you sure you want to ' + type + ' this transaction?')) {
                window.location.href = 'empTransactions.php?id=' + id + '&type=' + type + '&token=' +token;
            }
        }
    </script>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            You are logged in as: <span class="user"><?php echo $user->username; ?></span>.
            <a href="empLogin.php?logout=logout">Log out</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <div class="content">

        <h2>Transaction revisions</h2>
        <?php if ($success !== null) {
            echo '<p>' . $success . '</p>';
        }?>
        <p><?php if (sizeof($transactions) === 0) {
                echo 'There are no new transactions pending review.';
            } else {
                echo 'There are ' . sizeof($transactions) . ' new transactions</a> pending review.';
                echo '<div id="accordion">';
                foreach ($transactions as $transaction) {
                    echo '<h3>' . $transaction->id . ': €' . $transaction->amount . ' from ' .
                        $fromAccounts[$transaction->fromAcc] . ' to ' . $transaction->toAcc . '</h3>';
                    echo '<div><table>';
                    echo '<tr><th>Client:</th><td>' . $clients[$transaction->client] . '</td></tr>';
                    echo '<tr><th>Origin:</th><td>' . $fromAccounts[$transaction->fromAcc] . '</td></tr>';
                    echo '<tr><th>Destination:</th><td>' . $transaction->toAcc . '</td></tr>';
                    echo '<tr><th>Amount:</th><td>' . $transaction->amount . '</td></tr>';
                    echo '<tr><th>Name:</th><td>' . $transaction->name . '</td></tr>';
                    echo '<tr><th>Comment:</th><td>' . $transaction->comment . '</td></tr>';
                    echo '<tr><th>Date and time:</th><td>' . $transaction->timestamp . '</td></tr>';
                    echo '</table>';
                    echo '<button onclick="run(\'' . $transaction->id . '\', \'' . $_SESSION['token_value'] . '\', \'accept\')">Accept</button>';
                    echo '<button onclick="run(\'' . $transaction->id . '\', \'' . $_SESSION['token_value'] . '\', \'reject\')">Reject</button>';
                    echo '</input>';
                    echo '</div>';
                }
                echo '</div>';
            }?></p>
        <p><a href="employee.php"><button>Back to employee control panel</button></a></p>
    </div>
</div>

</body>
</html>
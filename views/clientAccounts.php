<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( " button" ).button();
            $( "input[type=submit]" ).button();
        });
    </script>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            You are logged in as: <span class="user"><?php
                if ($user->gender === 'male') {
                    echo 'Mr. ';
                } else {
                    echo 'Ms. ';
                }
                echo $user->name . ' ' . $user->surname;
                ?></span>. <a href="login.php?logout=1">Log out</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <div class="content">
        <h2>My bank accounts</h2>

        <?php if ($requestStatus === 'success') {
            echo '<h4 class="separate">Your new account is pending approval. It will appear here once it is approved by our staff.</h4>';
        } else if ($requestStatus === 'error') {
            if (isset($errorMsg)) {
                echo '<h4 class="separate error">' . $errorMsg . '</h4>';
            } else {
                echo '<h4 class="separate error">An error has occurred. Your request has not been processed.</h4>';
            }
        }?>

        <?php
        if (sizeof($accounts) > 0) {
            echo '<p>The following bank accounts are registered to your name:</p>
        <ul class="niceList">';
            foreach ($accounts as $a) {
                echo '<li>' . $a->numeration . '</li>';
            }
            echo '</ul>';
        } else {
            echo '<p>You do not have any accounts yet.</p>';
        }
        ?>

        <br class="separate">

        <h3 class="separate">Request a new account</h3>
        <p>If you want another account, you can request a new one here.</p>
        <form action="clientAccounts.php" method="post">
            <div class="formRow">
                <label for="tan">TAN #<?php echo $requestedTan; ?>:</label>
                <input type="number" name="tan" id="tan" maxlength="10">
            </div>
            <input type="submit" value="Request new account">
            <input type="hidden" name="token" value="<?php echo $_SESSION['token_value']; ?>" />
        </form>

        <br class="separate">
        <a href="clientMain.php"><button>Back to client area</button></a>
    </div>
</div>

</body>
</html>
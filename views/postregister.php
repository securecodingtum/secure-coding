<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            <ul id="menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Products</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <a href="login.php" id="clientLogin">Client login</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <div class="content">
        <img class="floatRight" src="img/placeholder.png">
        <h2>You are almost there!</h2>
        <p>In order to complete your registration with Servus Bank, a verification link has been sent to the e-mail
           address you provided. Please open the link with your web browser to complete your registration.</p>
        <?php if (isset($debugmsg)) {
            echo '<p id=debug>Debug: ' . $debugmsg . '</p>';
        } ?>
        <div class="clear"></div>
        <div class="footer">
            <a href="#">Legal information</a> ·
            <a href="#">Rates</a> ·
            <a href="#">Site map</a> ·
            <a href="empLogin.php">Employees</a>
        </div>
    </div>
</div>

</body>
</html>
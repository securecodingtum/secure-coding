<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script>
         $(function() {
            $( " button" ).button();
            $( "input[type=submit]" ).button();
        });
    </script>
</head>
<body>

	<div id="headerWrapper">
		<div id="header">
			<h2>
				<img src="img/logoSmall.png"> Servus Bank
			</h2>
			<div id="menuContainer">
				You are logged in as: <span class="user"><?php echo $user->username; ?>
				</span>. <a href="empLogin.php?logout=logout">Log out</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<div id="mainContainer">
		<div class="content">

			<h2>Initialize accounts</h2>
			<?php
			if (isset($success)) {
				echo '<p>' . $success . '</p>';
			}
			else if (isset($error)) {
				echo '<p class="error">' . $error . '</p>';
			}
			?>
			<p>Please complete the following fields to override the balance in an account:</p>
			<form action="cashDeposit.php" method="post">
				<div class="formRow">
					<label for="account">Account:</label> 
					<select name="account" id="account">
                    <?php foreach ($accounts as $a) {
                        echo '<option value="' . $a->numeration . '">' . $a->numeration . '</option>';
                    }; ?>
                </select>
				</div>
				<div class="formRow">
					<label for="amount">Amount:</label> <input type="text"
						name="amount" id="amount" maxlength="12" required>
				</div>
				<input type="submit" class="separate" value="Deposit"> <input
					type="hidden" name="token"
					value="<?php echo $_SESSION['token_value']; ?>" />
				<input type="hidden" name="userId"
					value="<?php echo $userId; ?>" />
			</form>
			<p>
				<a href="employee.php"><button>Back to employee control panel</button></a>
			</p>
		</div>
	</div>

</body>
</html>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            <ul id="menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Products</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <a href="login.php" id="clientLogin">Client login</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <img class="left" src="img/placeholder.png">
    <div class="middleContent">
        <h3>Your reasonably safe online bank account</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
            ea commodo consequat.</p>
        <a class="bigButton" href="register.php">Register now</a>
    </div>
    <img class="right" src="img/placeholder.png">
    <div class="footer">
        <a href="#">Legal information</a> ·
        <a href="#">Rates</a> ·
        <a href="#">Site map</a> ·
        <a href="empLogin.php">Employees</a>
    </div>
</div>

</body>
</html>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#accordion" ).accordion({
                collapsible: true
            });
            $('button').button();
        });
        var exec = function(id, username, token, type) {
            if (confirm('Are you sure you want to ' + type + ' this transaction?')) {
                window.location.href = 'empClients.php?id=' + id + '&user=' + username + '&type=' + type + '&token=' +token;
            }
        }
    </script>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/employee.css" rel="stylesheet" type="text/css">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            You are logged in as: <span class="user"><?php echo $user->username; ?></span>.
            <a href="empLogin.php?logout=logout">Log out</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <div class="content">

        <h2>Client revisions</h2>
        <?php if ($success !== null) {
            echo '<p>' . $success . '</p>';
        }?>
        <p><?php if (sizeof($clients) === 0) {
                echo 'There are no new clients pending review.';
            } else {
                echo 'There are ' . sizeof($clients) . ' new clients</a> pending review.';
                echo '<div id="accordion">';
                foreach ($clients as $client) {
                    echo '<h3>' . $client->id . ': ' . $client->surname . ', ' . $client->name . '</h3>';
                    echo '<div><table>';
                    echo '<tr><th>First name:</th><td>' . $client->name . '</td></tr>';
                    echo '<tr><th>Surname:</th><td>' . $client->surname . '</td></tr>';
                    echo '<tr><th>Gender:</th><td>' . ($client->gender === 'male' ? 'Male' : 'Female') . '</td></tr>';
                    echo '<tr><th>E-mail address:</th><td>' . $client->email . '</td></tr>';
                    echo '<tr><th>Username:</th><td>' . $client->username . '</td></tr>';
                    echo '<tr><th>Passport/ID document:</th><td>' . $client->passport . '</td></tr>';
                    echo '<tr><th>Address:</th><td>' . $client->address . '</td></tr>';
                    echo '</table>';
                    echo '<button onclick="exec(\'' . $client->id . '\', \'' . $client->username . '\', \'' . $_SESSION['token_value'] . '\', \'accept\')">Accept</button>';
                    echo '<button onclick="exec(\'' . $client->id . '\', \'' . $client->username . '\', \'' . $_SESSION['token_value'] . '\', \'reject\')">Reject</button>';
                    echo '</div>';
                }
                echo '</input>';
                echo '</div>';
            }?></p>
        <a href="employee.php"><button>Back to employee control panel</button></a>
    </div>
</div>

</body>
</html>
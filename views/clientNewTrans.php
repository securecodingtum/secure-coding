<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script>
         $(function() {
            $( " button" ).button();
            $( "input[type=submit]" ).button();
        });
    </script>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            You are logged in as: <span class="user"><?php
                if ($user->gender === 'male') {
                    echo 'Mr. ';
                } else {
                    echo 'Ms. ';
                }
                echo $user->name . ' ' . $user->surname;
                ?></span>. <a href="login.php?logout=1">Log out</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <div class="content">

        <h2>New transaction</h2>
        <?php
        if ($requestStatus === 'success') {
            echo '<h4 class="separate">Your transactions have been successfully completed!</h4>';
        } else if ($requestStatus === 'pending') {
            echo '<h4 class="separate">Your transactions are pending approval. They will be approved or rejected shortly.</h4>';
        } else if ($requestStatus === 'error') {
            if (isset($errorMsg)) {
                echo '<h4 class="separate error">An error has occurred: ' . $errorMsg . '</h4>';
            } else {
                echo '<h4 class="separate error">Your request could not be completed. No transactions have been performed.</h4>';
            }
        } else if ($requestStatus === 'batch') {
                        $failure = false;
			foreach ($errorMsgs as $msg) {
				if ($msg['status'] == 'success') {
					continue;
				} else if ($msg['status'] == 'pending') {
					echo '<h4 class="separate">Line ' . $msg['line'] . ': this transaction is pending approval.</h4>';
				} else {
                                        $failure = true;
					echo '<h4 class="separate error">Line ' . $msg['line'] . ': Error: ' . $msg['text'] . '</h4>';
				}
			}
			if (!$failure) {
                                echo '<h4 class="separate">All transactions were completed successfully.</h4>';
			}        }
        ?>

        <h3 class="separate">Single transaction</h3>
        <p>Please fill in the following fields to perform a single transaction:</p>
        <form action="clientNewTrans.php" method="post">
            <div class="formRow">
                <label for="from">From: </label>
                <select name="from" id="from">
                    <?php foreach ($accounts as $a) {
                        echo '<option value="' . $a->numeration . '">' . $a->numeration . '</option>';
                    }; ?>
                </select>
            </div>
            <div class="formRow">
                <label for="to">To:</label>
                <input type="text" name="to" id="to" maxlength="24">
            </div>
            <div class="formRow">
                <label for="name">Name:</label>
                <input type="text" name="name" id="name" maxlength="60">
            </div>
            <div class="formRow">
                <label for="amount">Amount:</label>
                <input type="text" name="amount" id="amount" maxlength="12">
            </div>
            <div class="formRow">
                <label for="comment">Comment:</label>
                <textarea name="comment" id="comment" maxlength="300" rows="5" cols="60"></textarea>
            </div>
            <div class="formRow">
                <input type="radio" name="tanType" value="mail" checked>e-mail TAN<br>
                <input type="radio" name="tanType" value="scs">SCS TAN
            </div>
            <div class="formRow">
                <label for="tan">TAN #<?php echo $requestedTan; ?>:</label>
                <input name="tan" id="tan" maxlength="15">
            </div>
            <input type="submit" name="submit" value="Perform">
            <input type="hidden" name="token" value="<?php echo $_SESSION['token_value']; ?>" />
        </form>

        <h3 class="separate">Multiple transactions</h3>
        <p>You can upload a CSV file containing information of multiple transactions here.</p>

        <form action="clientNewTrans.php" method="post" enctype="multipart/form-data">
            <div class="formRow">
                <label for="file">File:</label>
                <input type="file" name="file" id="file">
            </div>
            <div class="formRow">
                <input type="radio" name="tanType" value="mail" checked>e-mail TAN<br>
                <input type="radio" name="tanType" value="scs">SCS TAN
            </div>
            <div class="formRow">
                <label for="tan">TAN #<?php echo $requestedTan; ?>:</label>
                <input type="text" name="tan" id="singleTan" maxlength="15">
            </div>
            <input type="submit" name ="upload" value="Upload file">
            <input type="hidden" name="token" value="<?php echo $_SESSION['token_value']; ?>" />
        </form>

        <a href="clientMain.php"><button>Back to client area</button></a>
    </div>
</div>

</body>
</html>

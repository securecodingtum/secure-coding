<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <title>Servus Bank</title>
    <script>
        var confirmDelete = function(username) {
            if (confirm("Are you sure you want to delete user " + username + "?")) {
                window.location.href = 'adm.php?delete=' + username + '&token=' + "<?php echo $_SESSION['token_value']; ?>";
            }
        }
    </script>
</head>
<body>

<div id="headerWrapper">
    <div id="header">
        <h2>
            <img src="img/logoSmall.png">
            Servus Bank
        </h2>
        <div id="menuContainer">
            You are logged in as: <span class="user"><?php echo $user->username; ?></span>. <a href="admLogin.php?logout=logout">Log out</a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="mainContainer">
    <div class="content">
        <h2>Administration</h2>
        <?php if ($success !== null) {
            echo '<p>' . $success . '</p>';
        } else if ($error !== null) {
            echo '<p class="error">' . $error . '</p>';
        }
        ?>
        <?php if (sizeof($employees) === 0) {
            echo '<p>There are no registered employees yet.</p>';
        } else {
            echo '<p>The following employees are registered:</p>';
            echo '<table class="employees">';
            echo '<tr><th>Username</th><th>Name</th><th>Delete</th></tr>';
            foreach ($employees as $employee) {
                echo '<tr><td>' . $employee->username . '</td>';
                echo '<td>' . $employee->surname . ', ' . $employee->firstName . '</td>';
                echo '<td><a href="#" onclick="confirmDelete(\'' . $employee->username . '\')">Delete</a></td></tr>';
            }
            echo '</table>';
        } ?>
        <h3 class="separate">Add a new employee</h3>
        <form action="adm.php" method="post">
                <div class="formRow">
            <label for="username">Username:</label> <input type="text" name="username" id="username" maxlength="10" required></div>
                        <div class="formRow">
            <label for="password">Password:</label> <input type="password" name="password" id="password" maxlength="20" required></div>
                        <div class="formRow">
            <label for="firstName">First name:</label> <input type="text" name="firstName" id="firstName" maxlength="45" required></div>
                        <div class="formRow">
            <label for="surname">Surname:</label> <input type="text" name="surname" id="surname" maxlength="45" required></div>
            <input type="submit" name="submit" id="submit" value="Add">
            <input type="hidden" name="token" value="<?php echo $_SESSION['token_value']; ?>" />
        </form>
    </div>
</div>

</body>
</html>
<?php

require_once('includes/db.php');
require_once("includes/class.employee.php");
require_once("includes/class.user.php");
require_once('includes/sanitize.php');
require_once("includes/class.csrf.php");


session_start();

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isEmployee') {
    $_SESSION['from'] = "employee.php";
    header("Location: empLogin.php");
    exit;
}
$user = new Employee();
$user->load_info($_SESSION['username']);

$success = null;
$users = new Users();
$csrf = new Csrf();
if (isset($_GET['type']) && isset($_GET['user'])) {
	if(!$csrf->verifyToken()){
		$success = 'Error processing request.';
	}else{
		$_GET = sanitize_html($_GET);

		$editedUser = $users->get_by_username($_GET['user']);
		if ($editedUser !== null) {
			if ($_GET['type'] === 'accept') {
				if($editedUser->status != 'verified'){ //should allow to accept only those users which have clicked on verification link but were not previously approved. 
					$success = "Unable to accept this user.";
				}else{
						$editedUser->status = 'approved';
						$editedUser->store();
						$success = 'User accepted successfully.';
					}
			} else if ($_GET['type'] === 'reject') {
				if($editedUser->status === 'approved'){ // previously approved users should not be deleted from database.
					$success = 'Cannot remove previously approved user.';
				}else{
					$editedUser->delete();
					$success = 'User rejected successfully.';
				}
			}
		}
		$csrf->setToken();
	}
}

session_regenerate_id();
$clients = $users->get_verified();
require('views/empClients.php');
?>

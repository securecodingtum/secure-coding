<?php

require_once('includes/db.php');
require_once('includes/sanitize.php');
session_start();

require_once("includes/class.admin.php");
require_once("includes/class.csrf.php");
require_once('includes/class.captcha.php');

ini_set('session.use_only_cookies', true);
$captcha = (new Captcha())->init();

$csrf = new Csrf();
$admin = new Admin();
$error = null;
$msg = null;
if (isset($_POST['username']) && isset($_POST['password'])) {
    if ($captcha->check()->isValid()) {
        $_POST = sanitize_html($_POST);
        if ($admin->login($_POST['username'], $_POST['password'])) {
            session_regenerate_id();
            $_SESSION['username'] = $_POST['username'];
			//$_SESSION['isAdmin'] = true;
			$_SESSION['roleCheckFlag'] = 'isAdmin';
            header("Location: adm.php");
            exit;
        } else {
			if(isset($msg)){
				$error = $msg;
			}else{
				$error = "Wrong username or password.";
			}
            require('views/admLogin.php');
            exit;
        }
    } else {
        $error = "CAPTCHA validation failed.";
        require('views/admLogin.php');
        exit;
    }
}

if (isset($_GET['logout'])) {
    unset($_SESSION['username']);
	unset($_SESSION['roleCheckFlag']);
    //unset($_SESSION['isAdmin']);
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }
    session_destroy();
    $error = "You were successfully logged out.";
    require('views/admLogin.php');
    exit;
}

$csrf->setToken();
require('views/admLogin.php');
?>

<?php

require_once('includes/db.php');
require_once("includes/class.employee.php");
require_once("includes/class.user.php");
require_once("includes/class.transaction.php");
require_once("includes/class.account.php");

if(!isset($_SESSION))
	session_start();

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isEmployee') {
        $_SESSION['from'] = "employee.php";
        header("Location: empLogin.php");
        exit;
}
$user = new Employee();
$user->load_info($_SESSION['username']);

$users = new Users();
$numClients = sizeof($users->get_verified());
$transactions = new Transactions();
$numTransactions = sizeof($transactions->get_pending());
$accounts = new Accounts();
$numAccounts = sizeof($accounts->get_unconfirmed());

session_regenerate_id();
require('views/emp.php');
?>
<?php

session_start();
require_once('includes/db.php');
require_once("includes/class.transaction.php");
require_once("includes/class.transactionCode.php");
require_once("includes/class.user.php");
require_once("includes/class.account.php");
require_once('includes/sanitize.php');
require_once("includes/class.csrf.php");

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isUser') {
    header("Location: login.php");
    exit;
}
$user = new User();
$csrf = new Csrf();
$user->load_info($_SESSION['username']);

$tcodes = new TransactionCodes();

$requestStatus = null;

$accs = new Accounts();
$accounts = $accs->get_by_client($user->id);

do {
    /*
     * verify tan
     */
    if (!isset($_POST['tanType']) || !isset($_POST['tan'])) {
        break;
    }
    if ($_POST['tanType'] === 'mail') {
        $requestedTan = $_SESSION['requestedTan'];
        unset($_SESSION['requestedTan']);
        if (!$tcodes->verify_code($user->id, $requestedTan, $_POST['tan'])) {
            $errorMsg = 'the specified TAN was incorrect.';
            $requestStatus = 'error';
            break;
        }
    } else if ($_POST['tanType'] === 'scs') {
        if (isset($_FILES['file']) && file_exists($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
            if (!$tcodes->verify_scs_batch($_POST['tan'], $user->username, $_FILES['file']['tmp_name'])) {
                $errorMsg = 'the specified TAN was incorrect.';
                $requestStatus = 'error';
                break;
            }
        } else {
            if (!$tcodes->verify_scs($_POST['tan'], $user->username, $_POST['from'], $_POST['to'], $_POST['amount'])) {
                $errorMsg = 'the specified TAN was incorrect.';
                $requestStatus = 'error';
                break;
            }
        }
    } else {
        $errorMsg = 'the specified TAN was incorrect.';
        $requestStatus = 'error';
        break;
    }

    /*
     * batch file transactions
     */
    if (isset($_FILES['file']) && file_exists($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
        if (!$csrf->verifyToken()) {
            $requestStatus = 'error';
            $errorMsg      = 'Please try again.';
            break;
        } else {
            // parse batch file
            $tmpname = tempnam('/tmp', 'upload-csv-');
            $lines = file($_FILES['file']['tmp_name'], FILE_SKIP_EMPTY_LINES);
            foreach ($lines as $line) {
                file_put_contents($tmpname, htmlspecialchars($line), FILE_APPEND);
            }
            unlink($_FILES['file']['tmp_name']);
//             move_uploaded_file($_FILES['file']['tmp_name'], $tmpname);
            $output = shell_exec('./batch/parse ' . $tmpname . ' ' . $user->id);
            $errorMsgs = array();
            foreach (explode("\n", $output) as $msg) {
                $tmp = explode(": ", $msg);
                $line = $tmp[0];
                $text = $tmp[1];
                if ($line == "" || $text == "") {
                    continue;
                }
                if ($text == "transaction performed successfully.") {
                    array_push($errorMsgs, array("line" => $line, "status" => "success", "text" => $text));
                } else if ($text == "transaction waiting for approval.") {
                    array_push($errorMsgs, array("line" => $line, "status" => "pending", "text" => $text));
                } else {
                    array_push($errorMsgs, array("line" => $line, "status" => "error", "text" => $text));
                }
            }
            $requestStatus = 'batch';
            unlink($tmpname);
            break;
        }
    }

    /*
     * single transaction
     */
    if (isset($_POST['from']) && isset($_POST['to']) && isset($_POST['name']) && isset($_POST['amount']) && isset($_POST['comment'])) {
        if (!$csrf->verifyToken()) {
            $requestStatus = 'error';
            $errorMsg      = 'Please try again.';
            break;
        }
        $checks =            ctype_alnum($_POST['from'])   && ctype_alnum($_POST['to']);
        $checks = $checks && is_numeric($_POST['amount']) && $_POST['amount'] > 0;
        $checks = $checks && (strpos($_POST['amount'], '.') === false || (strpos($_POST['amount'], '.') >= strlen($_POST['amount']) - 3));
        if (!$checks) {
            if (!ctype_alnum($_POST['from']) || !ctype_alnum($_POST['to']) ) {
                $errorMsg = "the origin and destination accounts must consist only of numbers and letters.";
            }
            else if (!is_numeric($_POST['amount']) || $_POST['amount'] <= 0) {
                $errorMsg = "the transaction amount must be a positive number.";
            }
            else if (strpos($_POST['amount'], '.') < strlen($_POST['amount']) - 3) {
                $errorMsg = "the transaction amount can only contain up to two decimals.";
            }
            $requestStatus = 'error';
            break;
        }

        $_POST = sanitize_html($_POST);
        $trans = new Transaction();
        $trans->client  = $user->id;
        $trans->fromAcc = $_POST['from'];
        $trans->toAcc   = $_POST['to'];
        $trans->amount  = $_POST['amount'];
        $trans->name    = $_POST['name'];
        $trans->comment = $_POST['comment'];

        $foundFromAcc = false;
        foreach ($accounts as $account) {
            if ($account->numeration == $trans->fromAcc) {
                $foundFromAcc = true;
                break;
            }
        }
        if (!$foundFromAcc) {
            $requestStatus = 'error';
            break;
        }

        if (!$accs->exists($trans->toAcc)) {
            $errorMsg = 'the destination account is not a valid account.';
            $requestStatus = 'error';
            break;
        } else {
            $_POST = sanitize_html($_POST);
            $trans = new Transaction();
            $trans->client  = $user->id;
            $trans->fromAcc = $_POST['from'];
            $trans->toAcc   = $_POST['to'];
            $trans->amount  = $_POST['amount'];
            $trans->name    = $_POST['name'];
            $trans->comment = $_POST['comment'];

            $toAcc = $accs->get_by_numeration($trans->toAcc)[0];
            $fromAcc = $accs->get_by_numeration($trans->fromAcc)[0];
            $trans->fromAcc = $fromAcc->id;
            if ($fromAcc->amount < $_POST['amount']) {
                $errorMsg = 'There is not enough money in your account.';
                $requestStatus = 'error';
                break;
            }

            if ($_POST['amount'] >= 10000) {
                $trans->status = 'pending';
                $requestStatus = 'pending';
                $trans->store();
                break;
            } else {
                $trans->status = 'accepted';
                $fromAcc->amount -= $_POST['amount'];
                if ($toAcc !== null) {
                    $toAcc->amount += $_POST['amount'];
                    $toAcc->store();
                }
                $fromAcc->store();
            }

            $trans->store();
            $requestStatus = 'success';
        }
    }
} while (0);
if (!isset($_SESSION['requestedTan'])) {
    $_SESSION['requestedTan'] = $tcodes->get_random($user->id);
}
$requestedTan = $_SESSION['requestedTan'];

session_regenerate_id();
$csrf->setToken();
require('views/clientNewTrans.php');

?>

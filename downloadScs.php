<?php

session_start();
require_once('includes/db.php');
require_once("includes/class.user.php");
require_once("includes/class.transactionCode.php");

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isUser') {
	header("Location: login.php");
	exit;
}
$user = new User();
$user->load_info($_SESSION['username']);

$tc = new TransactionCodes();

$username = $user->username;
$secret = $tc->get_secret($username);

$tmpname = tempnam('/tmp', 'scs-');
unlink($tmpname);
mkdir($tmpname);

$files = array("Main.java", "Main.form", "manifest.mf", "swing-layout-1.0.4.jar");
foreach ($files as $file) {
        copy("./includes/scs/tmp/" . $file, $tmpname . "/" . $file);
}

# replace username and secret
$output = shell_exec("perl -pi -e 's/private String secretCode = \"\";/private String secretCode = \"$secret\";/' $tmpname/Main.java");
$output = shell_exec("perl -pi -e 's/private String username = \"\";/private String username = \"$username\";/' $tmpname/Main.java");

# build scs
$output = shell_exec("cd $tmpname && javac Main.java");
$output = shell_exec("cd $tmpname && jar cmf manifest.mf SCS.jar *.class swing-layout-1.0.4.jar");
$out_file = $tmpname . "/SCS.jar";

$fp = fopen($out_file, 'rb');
header("Content-Type: application/java-archive");
header("Content-Length: " . filesize($out_file));
header('Content-Disposition: attachment; filename=SCS.jar');
header('Content-Transfer-Encoding: binary');

fpassthru($fp);
exit;

?>
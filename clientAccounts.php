<?php

session_start();
require_once('includes/db.php');
require_once("includes/class.account.php");
require_once("includes/class.user.php");
require_once("includes/class.transactionCode.php");
require_once("includes/class.csrf.php");
require_once('includes/sanitize.php');

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isUser') {
	header("Location: login.php");
	exit;
}
$user = new User();
$csrf = new Csrf();
$user->load_info($_SESSION['username']);

$requestStatus = null;

$tcodes = new TransactionCodes();

$accs = new Accounts();

if (isset($_POST['tan'])) {
	if(!$csrf->verifyToken()){
			$requestStatus = 'error';
	}else{
		$_POST = sanitize_html($_POST);
        $requestedTan = $_SESSION['requestedTan'];
        unset($_SESSION['requestedTan']);
		if ($tcodes->verify_code($user->id, $requestedTan, $_POST['tan'])) {
			$requestStatus = 'success';
			$accs->create($_SESSION['username']);
		} else {
            $errorMsg = 'The specified TAN was incorrect.';
            $requestStatus = 'error';
		}
		$csrf->setToken();
	}
}

$accounts = $accs->get_by_client($user->id);

if (!isset($_SESSION['requestedTan'])) {
    $_SESSION['requestedTan'] = $tcodes->get_random($user->id);
}
$requestedTan = $_SESSION['requestedTan'];
session_regenerate_id();
$csrf->setToken();
require('views/clientAccounts.php');
?>

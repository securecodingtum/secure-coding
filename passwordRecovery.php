<?php

require_once('includes/db.php');
require_once("includes/class.user.php");
require_once('includes/sanitize.php');
require_once('includes/class.csrf.php');

$csrf = new Csrf();
$error = null;
session_start();
if (isset($_POST['username']) && isset($_POST['passport']) && isset($_POST['email'])) {
	if(!$csrf->verifyToken()){
		$error = 'There was an error while processing your request.';
	} else {
		$user = new User();
		$_POST = sanitize_html($_POST);
		$user->load_info($_POST['username']);
		if($user->username === $_POST['username']
				&& $user->passport ===$_POST['passport']
				&& $user->email === $_POST['email']){

			if ($user->status === 'approved' || $user->status === 'verified') {
				$password = generatePassword();
				$user->set_password($password);
				$user->updatePassword();
				require_once('includes/class.mail.php');
				$title = "Your new password";
				$body = "<h2>Servus Bank</h2><p>This is your new password. Please keep it safe!</p>";
				$body .= '<p>';
				$body .= $password;
				$body .= '</p>';
				$mailer = new Mail();
				if ($mailer->send($title, $body, $user->email)) {
					$success = "Your password has been changed successfully. You will soon receive an e-mail with your new password.";
				} else {
					$error = "There was an error while trying to change your password. If the problem persists, please contact our staff.";
				}
					
			} else if ($user->status === 'unverified') {
				$error = "Your e-mail needs to be verified before you can use this functionality.";
			} else {
				$error = "Your account is currently unavailable. Please contact our staff for more information.";
			}
		} else {
			$error = "The entered values are not correct.";
		}
	}
}

function generatePassword(){
	$length = mt_rand(6, 20);
	$string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,:!$';
	return substr(str_shuffle($string), 0, $length);
}

$csrf->setToken();
require('views/passwordRecovery.php');
?>

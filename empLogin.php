<?php

session_start();
require_once('includes/db.php');
require_once("includes/class.employee.php");
require_once('includes/sanitize.php');
require_once("includes/class.csrf.php");
require_once('includes/class.captcha.php');
$csrf = new Csrf();

ini_set('session.use_only_cookies', true);
$captcha = (new Captcha())->init();
$success = null;
$error   = null;

if (isset($_POST['username']) && isset($_POST['password'])) {
    $_POST = sanitize_html($_POST);
    $user = new Employee();
    $user->load_info($_POST['username']);
	$timestamp = date('Y-m-d G:i:s');
 
    if ($captcha->check()->isValid()) {
		if($user->verify_password($_POST['password'])){
			if($user->getInvalidAttempts() >=3 && time()-strtotime($user->getTimestamp()) < 3600){
				$error = "Account is locked. Attempt after sometime";
				require('views/empLogin.php');
				exit;
			}elseif($user->getInvalidAttempts()>0){
				$user->updateLoginAttempts(0,$timestamp,$user->username);
			}
			$_SESSION['username']   = $_POST['username'];
			//$_SESSION['isEmployee'] = true;
			$_SESSION['roleCheckFlag'] = 'isEmployee';
			$csrf->setToken();
			header("Location: employee.php");
			exit;
		}else {
			$user->updateLoginAttempts($user->getInvalidAttempts()+1,$timestamp);
			$error = "Username or password were wrong.";
			require('views/empLogin.php');
			exit;
		}
    } else {
        $error = "CAPTCHA validation failed.";
        require('views/empLogin.php');
        exit;
    }
}

if (isset($_GET['logout'])) {
    unset($_SESSION['username']);
    //unset($_SESSION['isEmployee']);
	unset($_SESSION['roleCheckFlag']);
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }
    session_destroy();
    $success = "You were successfully logged out";
}

$csrf->setToken();
require('views/empLogin.php');
?>

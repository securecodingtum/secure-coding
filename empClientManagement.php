<?php
require_once('includes/db.php');
require_once("includes/class.employee.php");
require_once("includes/class.user.php");
require_once('includes/class.csrf.php');
require_once('includes/sanitize.php');
require_once("includes/class.account.php");
require_once("includes/class.transaction.php");

session_start();
$csrf = new Csrf();

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isEmployee') {
	$_SESSION['from'] = "employee.php";
	header("Location: empLogin.php");
	exit;
}
if(isset($_POST['username'])){
	if(!$csrf->verifyToken()){
		$error = 'Your request could not be processed.';
	}else {
		$_POST = sanitize_html($_POST);
		$user = new User();
		$user->load_info($_POST['username']);
		if(is_null($user->username)){
			$error = 'No user with that username exists.';
		}elseif (isset($_POST['viewTrans'])) {
			require_once 'clientViewTrans.php';
			exit;
		}else {
			$accs = new Accounts();
			$accountNames = array();
			$userId = $user->id;
			$accounts = $accs->get_by_client($userId);
			require_once 'cashDeposit.php';
			exit;
		}
	}
}
$csrf->setToken();
require 'employee.php';

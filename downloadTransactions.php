<?php
$error = null;
session_start();
require_once('includes/db.php');
require_once("includes/class.account.php");
require_once("includes/class.transaction.php");
require_once("includes/class.user.php");
require_once("includes/class.csrf.php");
require_once('includes/html2pdf_v4.03/html2pdf.class.php');

if (!isset($_SESSION['username']) || !isset($_SESSION['roleCheckFlag'])  || $_SESSION['roleCheckFlag'] != 'isUser') {
	header("Location: login.php");
	exit;
}
$user = new User();
$user->load_info($_SESSION['username']);

$accs = new Accounts();
$accountNames = array();
$accounts = $accs->get_by_client($user->id);
foreach ($accounts as $account) {
	$accountNames[$account->id] = $account->numeration;
}
$requestStatus = null;
$t = new Transactions();
$transactions = $t->get_by_client($user->id);
error_log("user id: " . $user->id);

//start the buffer and also clean it to remove previous contents (if any)
ob_start();
ob_clean();

?>
<!-- Generate the output format of pdf file
	Example, get the bank logo and name along with the 
	format in which content should be displayed to the user in pdf. -->
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">


<body>
	<div id="headerWrapper">
		<div id="header">
			<h2>
				<img src="img/logoSmall.png"> Servus Bank
			</h2>
			<div class="clear"></div>
		</div>
	</div>
	<div id="mainContainer">
		<div class="content">

			<h2>Transactions</h2>

			<table>
				<thead>
					<tr>
						<th>Date and time</th>
						<th>From</th>
						<th>To</th>
						<th>Name</th>
						<th>Amount</th>
						<th>Status</th>
						<th>Comment</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($transactions as $transaction) {
						echo '<tr>';
						echo "<td>" . $transaction->timestamp . "</td>";
						echo "<td>" . $accountNames[$transaction->fromAcc] . "</td>";
						echo "<td>" . $transaction->toAcc . "</td>";
						echo "<td>" . $transaction->name . "</td>";
						echo "<td>" . $transaction->amount . "</td>";
						echo '<td>' . (($transaction->status === 'accepted') ? 'Accepted' : 'Pending') . '</td>';
						echo "<td>" . $transaction->comment . "</td>";
						echo "</tr>\n";
					};
					?>
				</tbody>
			</table>
		</div>
	</div>

</body>
</html>
<?php 
/*
 * get the buffer contents and then clean the buffer
* after that call html2pdf to generate a pdf and
* force download it
* User will be able to print it but copy is not allowed.
*/
$output = ob_get_contents();
ob_clean();
$html2pdf = new HTML2PDF('P', 'A4', 'en');
$html2pdf->pdf->SetProtection(array('print'), '');
$html2pdf->writeHTML($output);
$html2pdf->Output('transactions.pdf','D');
?>

